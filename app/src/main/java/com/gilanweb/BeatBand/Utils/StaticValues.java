package com.gilanweb.BeatBand.Utils;

import com.gilanweb.BeatBand.Retrofit.Model.Beat;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by meghdadya on 11/7/16.
 */

public class StaticValues {
    public static String SlidesUrl = "http://mybeatband.com/dashboard/images/slide/";
    public static String CoverUrl = "http://mybeatband.com/dashboard/images/cover/";
    public static String MusicUrl = "http://mybeatband.com/dashboard/images/place/";
    public static String AdvUrl="http://mybeatband.com/dashboard/images/link/";
    public static List<Beat> beatArrayList = new ArrayList<>();
    public static Long songIndex;

    public static Beat getmCurrentSong() {
        return mCurrentSong;
    }

    public static void setmCurrentSong(Beat mCurrentSong) {
        StaticValues.mCurrentSong = mCurrentSong;
    }

    public static Beat mCurrentSong;


    public static List<Beat> getBeatArrayList() {
        return beatArrayList;
    }

    public static void setBeatArrayList(List<Beat> beatArrayList) {
        StaticValues.beatArrayList = beatArrayList;
    }

    public static Long getSongIndex() {
        return songIndex;
    }

    public static void setSongIndex(Long songIndex) {
        StaticValues.songIndex = songIndex;
    }


}
