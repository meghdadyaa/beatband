package com.gilanweb.BeatBand.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class DatePreferences {
    // Shared Preferences
    SharedPreferences pref;

    Editor editor;
    Context context;

    // Shared pref mode
    int privateMode = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "BeatBand";

    private static final String id_list = "tag_ids";

    private static final String price = "tag_price";

    private static final String time = "tag_time";

    public DatePreferences(Context context) {
        this.context = context;
        pref = this.context.getSharedPreferences(PREF_NAME, privateMode);
        editor = pref.edit();
    }

    public void setTagIds(String s) {
        editor.putString(id_list, s);
        // commit changes
        editor.commit();

    }

    public String getTagsIds() {
        return pref.getString(id_list, "0");
    }


    public String getTagPrice() {
        return pref.getString(price, "0");
    }

    public void SetTagPrice(String s) {
        editor.putString(price, s);
        // commit changes
        editor.commit();
    }

    public String getTagTime() {
        return pref.getString(time, "new");
    }

    public void SetTagTime(String s) {
        editor.putString(time, s);
        // commit changes
        editor.commit();
    }


}