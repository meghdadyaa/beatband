package com.gilanweb.BeatBand.Utils;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import com.gilanweb.BeatBand.Retrofit.Model.Beat;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by meghdadya on 1/31/17.
 */

public class MyDatabase extends SQLiteAssetHelper {

    private static final String DATABASE_NAME = "Beatbox.db";
    private static final int DATABASE_VERSION = 1;

    public MyDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void insertBeat(final Beat beat) {
        AsyncTask<Object, Object, Object> saveContactAsyncTask;
        saveContactAsyncTask = new AsyncTask<Object, Object, Object>() {
            @Override
            protected Object doInBackground(Object... params) {

                try {
                    SQLiteDatabase db = getReadableDatabase();

                    Cursor c = db.rawQuery("INSERT INTO BeatDb(Id,Duration, beat,cover,created_at,name,price,sold,special,updated_at,fileprice)VALUES('" + beat.getId() + "','" + beat.getDuration() + "','" + beat.getBeat() + "','" + beat.getCover() + "','" + beat.getWave_price() + "','" + beat.getName() + "','" + beat.getWave_price() + "','" + beat.getSold() + "','" + beat.getSpecial() + "','" + beat.getUpdated_at() + "','"+beat.getFile_price()+"')", null);
                    c.moveToFirst();

                } finally {

                }
                return null;
            }
        };
        // Execute the saveContactAsyncTask AsyncTask above
        saveContactAsyncTask.execute((Object[]) null);


    }

    public Boolean selectBeat(String number) {

        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * from BeatDb WHERE Id='" + number + "'", null);
        if (c.getCount() != 0) {
            return true;
        } else {
            return false;
        }
    }


    public void deletebeat(String id) {

        SQLiteDatabase db = getReadableDatabase();

        Cursor c = db.rawQuery("DELETE FROM BeatDb where Id=" + id + "", null);
        c.moveToFirst();


    }


    public ArrayList<String> reternIds() {

        ArrayList<String> ids = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery("SELECT Id FROM BeatDb", null);
        if (c.moveToFirst()) {
            for (int i = 0; c.getCount() > i; i++) {
                ids.add(c.getString(0));
                c.moveToNext();
            }
        }
        c.close();
        return ids;

    }

    public List<Beat> getAllPlayList() {
        List<Beat> BeatList = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM BeatDb", null);
        if (c.moveToFirst()) {
            for (int i = 0; c.getCount() > i; i++) {
                Beat beat = new Beat();
                beat.setId(c.getString(0));
                beat.setDuration(c.getString(1));
                beat.setBeat(c.getString(2));
                beat.setCover(c.getString(3));
                beat.setCretaed_at(c.getString(4));
                beat.setName(c.getString(5));
                beat.setWave_price(c.getString(6));
                beat.setSold(c.getString(7));
                beat.setSpecial(c.getString(8));
                beat.setUpdated_at(c.getString(9));
                beat.setFile_price(c.getString(10));
                BeatList.add(beat);
                c.moveToNext();
            }
        }
        c.close();
        return BeatList;
    }


}
