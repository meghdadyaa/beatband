package com.gilanweb.BeatBand.Utils;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.support.v7.app.AlertDialog;
import android.widget.EditText;
import android.widget.Toast;

import com.gilanweb.BeatBand.services.MusicService;

import java.util.Locale;

public class Utils {
    public static void sendIntent(Context context, String action) {
        final Intent intent = new Intent(context, MusicService.class);
        intent.setAction(action);
        context.startService(intent);
    }

    public static Bitmap getColoredIcon(Resources res, int newColor, int id) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inMutable = true;
        final Bitmap bmp = BitmapFactory.decodeResource(res, id, options);
        final Paint paint = new Paint();
        final ColorFilter filter = new PorterDuffColorFilter(newColor, PorterDuff.Mode.SRC_IN);
        paint.setColorFilter(filter);
        final Canvas canvas = new Canvas(bmp);
        canvas.drawBitmap(bmp, 0, 0, paint);
        return bmp;
    }

    public static void showToast(Context context, int resId) {
        Toast.makeText(context, context.getResources().getString(resId), Toast.LENGTH_SHORT).show();
    }

    public static String getFilename(final String path) {
        return path.substring(path.lastIndexOf("/") + 1);
    }

    public static String getViewText(EditText editText) {
        return editText.getText().toString().trim();
    }

    public static String getTimeString(int duration) {
        final StringBuilder sb = new StringBuilder(8);
        final int hours = duration / (60 * 60);
        final int minutes = (duration % (60 * 60)) / 60;
        final int seconds = ((duration % (60 * 60)) % 60);

        if (duration > 3600) {
            sb.append(String.format(Locale.getDefault(), "%02d", hours)).append(":");
        }

        sb.append(String.format(Locale.getDefault(), "%02d", minutes));
        sb.append(":").append(String.format(Locale.getDefault(), "%02d", seconds));

        return sb.toString();
    }
    public static Bitmap createBitmap_ScriptIntrinsicBlur(Context context, Bitmap src, float r) {

        if (r <= 0) {
            r = 0.1f;
        } else if (r > 25) {
            r = 25.0f;
        }


        Bitmap copy = src.copy(Bitmap.Config.ARGB_8888, true);
        Bitmap bitmap = Bitmap.createScaledBitmap(copy,
                src.getWidth(), src.getHeight(),
                false);

        android.support.v8.renderscript.RenderScript renderScript = android.support.v8.renderscript.RenderScript.create(context);

        android.support.v8.renderscript.Allocation blurInput = android.support.v8.renderscript.Allocation.createFromBitmap(renderScript, src);
        android.support.v8.renderscript.Allocation blurOutput = android.support.v8.renderscript.Allocation.createFromBitmap(renderScript, bitmap);

        android.support.v8.renderscript.ScriptIntrinsicBlur blur = android.support.v8.renderscript.ScriptIntrinsicBlur.create(renderScript,
                android.support.v8.renderscript.Element.U8_4(renderScript));
        blur.setInput(blurInput);
        blur.setRadius(r);
        blur.forEach(blurOutput);

        blurOutput.copyTo(bitmap);
        renderScript.destroy();
        return bitmap;
    }



}
