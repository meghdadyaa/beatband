package com.gilanweb.BeatBand.Retrofit.Model;

public class Allprice {

    private String id;

    private String updated_at;

    private String price;

    private String created_at;

    private String lable;

    private boolean isSelected = false;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getUpdated_at ()
    {
        return updated_at;
    }

    public void setUpdated_at (String updated_at)
    {
        this.updated_at = updated_at;
    }

    public String getPrice ()
    {
        return price;
    }

    public void setPrice (String price)
    {
        this.price = price;
    }

    public String getCreated_at ()
    {
        return created_at;
    }

    public void setCreated_at (String created_at)
    {
        this.created_at = created_at;
    }

    public String getLable ()
    {
        return lable;
    }

    public void setLable (String lable)
    {
        this.lable = lable;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", updated_at = "+updated_at+", price = "+price+", created_at = "+created_at+", lable = "+lable+"]";
    }


    public void setSelected(boolean selected) {
        isSelected = selected;
    }


    public boolean isSelected() {
        return isSelected;
    }
}
