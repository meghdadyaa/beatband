package com.gilanweb.BeatBand.Retrofit.Model;

import java.util.ArrayList;

public class Order {

    private ArrayList<OrderOrders> orders;

    public ArrayList<OrderOrders> getOrders() {
        return orders;
    }

    public void setOrders(ArrayList<OrderOrders> orders) {
        this.orders = orders;
    }


}
