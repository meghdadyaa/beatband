package com.gilanweb.BeatBand.Retrofit.Model;

public class OrderOrders {

    private String ref_id;
    private String shop_date;
    private String updated_at;
    private String user_id;
    private String price;
    private String created_at;
    private String id;
    private String beat_id;
    private String shop_time;
    private String seen;
    private String status;


    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getRef_id() {
        return this.ref_id;
    }

    public void setRef_id(String ref_id) {
        this.ref_id = ref_id;
    }

    public String getShop_date() {
        return this.shop_date;
    }

    public void setShop_date(String shop_date) {
        this.shop_date = shop_date;
    }

    public String getUpdated_at() {
        return this.updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getUser_id() {
        return this.user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getPrice() {
        return this.price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCreated_at() {
        return this.created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBeat_id() {
        return this.beat_id;
    }

    public void setBeat_id(String beat_id) {
        this.beat_id = beat_id;
    }

    public String getShop_time() {
        return this.shop_time;
    }

    public void setShop_time(String shop_time) {
        this.shop_time = shop_time;
    }

    public String getSeen() {
        return this.seen;
    }

    public void setSeen(String seen) {
        this.seen = seen;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
