package com.gilanweb.BeatBand.Retrofit.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Recommended {

    @SerializedName("recomended")
    @Expose
    private List<Beat> recomended;

    public List<Beat> getRecomended() {
        return recomended;
    }

    public void setRecomended(List<Beat> recomended) {
        this.recomended = recomended;
    }
}
