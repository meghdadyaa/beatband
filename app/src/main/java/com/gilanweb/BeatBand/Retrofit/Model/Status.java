package com.gilanweb.BeatBand.Retrofit.Model;

public class Status {
    private String status;

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
