
package com.gilanweb.BeatBand.Retrofit.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;



public class Adv {

    @SerializedName("created_at")
    @Expose
    private String mCreatedAt;
    @SerializedName("id")
    @Expose
    private Long mId;
    @SerializedName("image")
    @Expose
    private String mImage;
    @SerializedName("link")
    @Expose
    private String mLink;
    @SerializedName("updated_at")
    @Expose
    private String mUpdatedAt;

    public String getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(String created_at) {
        mCreatedAt = created_at;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getImage() {
        return mImage;
    }

    public void setImage(String image) {
        mImage = image;
    }

    public String getLink() {
        return mLink;
    }

    public void setLink(String link) {
        mLink = link;
    }

    public String getUpdatedAt() {
        return mUpdatedAt;
    }

    public void setUpdatedAt(String updated_at) {
        mUpdatedAt = updated_at;
    }

}
