
package com.gilanweb.BeatBand.Retrofit.Model;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;



public class GetAdv {

    @SerializedName("slides")
    @Expose
    private List<Adv> mSlides;

    public List<Adv> getSlides() {
        return mSlides;
    }

    public void setSlides(List<Adv> slides) {
        mSlides = slides;
    }

}
