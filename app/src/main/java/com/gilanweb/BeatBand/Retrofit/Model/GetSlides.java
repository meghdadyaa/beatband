package com.gilanweb.BeatBand.Retrofit.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by meghdadya on 1/7/17.
 */

public class GetSlides {

    @SerializedName("slides")
    @Expose
    private List<Slides> slidesList;

    public List<Slides> getSlidesList() {
        return slidesList;
    }

    public void setSlidesList(List<Slides> slidesList) {
        this.slidesList = slidesList;
    }



}
