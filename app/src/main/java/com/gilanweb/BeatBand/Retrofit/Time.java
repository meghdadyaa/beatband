package com.gilanweb.BeatBand.Retrofit;

public class Time {

    private String time;
    private boolean isSelected = false;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
