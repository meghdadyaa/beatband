package com.gilanweb.BeatBand.Retrofit.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by meghdadya on 11/4/16.
 */

public class OtherBeats {
    @SerializedName("otherbeat")
    @Expose
    private List<Beat> beatList;
    @SerializedName("genre")
    @Expose
    private ArrayList<Genre> genre;
    @SerializedName("allprice")
    @Expose
    private List<Allprice> allprice;

    public List<Beat> getBeatList() {
        return beatList;
    }

    public void setBeatList(List<Beat> beatList) {
        this.beatList = beatList;
    }

    public ArrayList<Genre> getGenre() {
        return genre;
    }

    public void setGenre(ArrayList<Genre> genre) {
        this.genre = genre;
    }

    public List<Allprice> getAllprice() {
        return allprice;
    }

    public void setAllprice(List<Allprice> allprice) {
        this.allprice = allprice;
    }
}
