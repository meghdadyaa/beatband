
package com.gilanweb.BeatBand.Retrofit.Model;




import java.util.List;


public class Beat {

    private String beat;

    private List<Cate> cate;

    public List<Cate> getCate() {
        return cate;
    }

    public void setCate(List<Cate> cate) {
        this.cate = cate;
    }

    private String file_price;

    private String cretaed_at;

    private String id;

    private String cover;

    private String layer_price;

    private String duration;

    private String updated_at;

    private String name;

    private String special;

    private String wave_price;

    private String sold;

    public String getBeat() {
        return beat;
    }

    public void setBeat(String beat) {
        this.beat = beat;
    }


    public String getFile_price() {
        return file_price;
    }

    public void setFile_price(String file_price) {
        this.file_price = file_price;
    }

    public String getCretaed_at() {
        return cretaed_at;
    }

    public void setCretaed_at(String cretaed_at) {
        this.cretaed_at = cretaed_at;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getLayer_price() {
        return layer_price;
    }

    public void setLayer_price(String layer_price) {
        this.layer_price = layer_price;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpecial() {
        return special;
    }

    public void setSpecial(String special) {
        this.special = special;
    }

    public String getWave_price() {
        return wave_price;
    }

    public void setWave_price(String wave_price) {
        this.wave_price = wave_price;
    }

    public String getSold() {
        return sold;
    }

    public void setSold(String sold) {
        this.sold = sold;
    }

    @Override
    public String toString() {
        return "ClassPojo [beat = " + beat + ", cate = " + cate + ", file_price = " + file_price + ", cretaed_at = " + cretaed_at + ", id = " + id + ", cover = " + cover + ", layer_price = " + layer_price + ", duration = " + duration + ", updated_at = " + updated_at + ", name = " + name + ", special = " + special + ", wave_price = " + wave_price + ", sold = " + sold + "]";
    }
}
