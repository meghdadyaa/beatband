package com.gilanweb.BeatBand.Retrofit.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CheckExpaire {
    @SerializedName("login-expire")
    @Expose
    private String loginexpire;

    public String getLoginexpire() {
        return this.loginexpire;
    }

    public void setLoginexpire(String loginexpire) {
        this.loginexpire = loginexpire;
    }
}
