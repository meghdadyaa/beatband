package com.gilanweb.BeatBand.Retrofit.Model;


import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;


public interface ApiInterface {
    @FormUrlEncoded
    @POST("specialbeats")
    Call<SpecialBeats> getAll(@Field("id") int id);

    @FormUrlEncoded
    @POST("otherbeats")
    Call<OtherBeats> getother(@Field("last_id") int last_id, @Field("genre_id") String genre_id, @Field("max_price") String max_price, @Field("order_by") String order_by);

    @FormUrlEncoded
    @POST("apigmail")
    Call<Token> getKey(@Field("gmail") String gmail);

    @GET("slides")
    Call<GetSlides> getSlides();

    @GET("links")
    Call<GetAdv> getAvd();

    @FormUrlEncoded
    @POST("apiregister")
    Call<Status> postRegister(@Field("name") String name, @Field("email") String email, @Field("password") String password, @Field("mobile") String mobile);

    @FormUrlEncoded
    @POST("apilogin")
    Call<LoginResponse> postlogin(@Field("email") String email, @Field("password") String password);

    @GET("apiorders")
    Call<Order> getOrders(@Header("logintoken") String token);

    @FormUrlEncoded
    @POST("checklist")
    Call<ResponseBody> postIds(@Field("ids") String ids);

    @FormUrlEncoded
    @POST("login-ex-check")
    Call<CheckExpaire> logincheck(@Field("logintoken") String ids);


    @FormUrlEncoded
    @POST("recommended")
    Call<Recommended> getRecommended(@Field("genre_id") String genres);

    @FormUrlEncoded
    @POST("getversion")
    Call<Getversion> getVersion(@Field("version")String version);



}
