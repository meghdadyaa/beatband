package com.gilanweb.BeatBand.Retrofit.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


/**
 * Created by meghdadya on 11/4/16.
 */

public class SpecialBeats {
    @SerializedName("specialbeats")
    @Expose
    private List<Beat> beatList;

    public List<Beat> getBeatList() {
        return beatList;
    }

    public void setBeatList(List<Beat> beatList) {
        this.beatList = beatList;
    }


}
