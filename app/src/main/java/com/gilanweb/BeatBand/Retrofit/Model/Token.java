package com.gilanweb.BeatBand.Retrofit.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by meghdadya on 2/7/17.
 */

public class Token {

    @SerializedName("login_token")
    @Expose
    private String login_token;

    public String getLogin_token ()
    {
        return login_token;
    }

    public void setLogin_token (String login_token)
    {
        this.login_token = login_token;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [login_token = "+login_token+"]";
    }
}
