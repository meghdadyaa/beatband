package com.gilanweb.BeatBand.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gilanweb.BeatBand.R;
import com.gilanweb.BeatBand.Retrofit.Model.Allprice;
import com.gilanweb.BeatBand.Retrofit.Time;
import com.gilanweb.BeatBand.Utils.DatePreferences;

import java.util.ArrayList;
import java.util.List;

public class TimeAdapter extends RecyclerView.Adapter<TimeAdapter.RecyclerViewHolder> {


    public List<Time> timeList;
    View view;
    Context context;
    DatePreferences preferences;

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {


        TextView textView;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.tag_id);


        }
    }


    public TimeAdapter(Context context) {
        timeList = new ArrayList<>();
        this.context = context;
        preferences = new DatePreferences(context);
    }

    @Override
    public TimeAdapter.RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_genre, parent, false);
        TimeAdapter.RecyclerViewHolder recyclerViewHolder = new TimeAdapter.RecyclerViewHolder(itemView);
        view = itemView;
        return recyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {
        final Time time = timeList.get(position);
        holder.textView.setText("#" + time.getTime());
        holder.textView.setTextColor(timeList.get(position).isSelected() ? ContextCompat.getColor(context, R.color.selected_tag_text_color) : ContextCompat.getColor(context, R.color.tag_text_color));
        if (timeList.get(position).isSelected()) {
            holder.itemView.setBackground(context.getResources().getDrawable(R.drawable.background_tag_item));
        } else {
            holder.itemView.setBackgroundColor(Color.TRANSPARENT);
        }

        holder.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                for (int k = 0; k < timeList.size(); k++) {
                    if (k == position) {
                        timeList.get(k).setSelected(true);
                        preferences.SetTagTime(time.getTime());
                    } else {
                        timeList.get(k).setSelected(false);

                    }
                }
                notifyDataSetChanged();

            }
        });


    }


    @Override
    public int getItemCount() {
        return timeList.size();
    }

    public void add(List<Time> items) {

        int previousDataSize = this.timeList.size();

        if (previousDataSize == 0) {
            timeList.addAll(items);
            notifyItemInserted(1);
        } else {
            timeList.addAll(items);
            notifyItemRangeInserted(previousDataSize, items.size());
        }


    }
}
