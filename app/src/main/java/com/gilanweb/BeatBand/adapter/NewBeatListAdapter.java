package com.gilanweb.BeatBand.adapter;


import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.Target;
import com.gilanweb.BeatBand.R;
import com.gilanweb.BeatBand.Retrofit.Model.Beat;
import com.gilanweb.BeatBand.Utils.StaticValues;

import java.util.List;


public class NewBeatListAdapter extends RecyclerView.Adapter<NewBeatListAdapter.RecyclerViewHolder> {

    public List<Beat> BeatList;
    Activity context;
    View view;

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {


        ImageView img;
        TextView code;
        CardView cardCatItem;
        FrameLayout frameLayout;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            frameLayout = itemView.findViewById(R.id.fr_new_beat);
            img = itemView.findViewById(R.id.beat_image);
            code = itemView.findViewById(R.id.music_code);
            Display display = context.getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int width = size.x;
            int height = size.y;
            frameLayout.getLayoutParams().width = ((size.x / 2) -42);
            frameLayout.getLayoutParams().height = frameLayout.getLayoutParams().width;
        }
    }

    public NewBeatListAdapter(List<Beat> beats, Activity context) {
        this.BeatList = beats;
        this.context = context;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.new_item_beat, parent, false);
        RecyclerViewHolder recyclerViewHolder = new RecyclerViewHolder(itemView);
        view = itemView;
        return recyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        Beat beat = BeatList.get(position);
        holder.code.setText(beat.getName());
        Glide
                .with(context)
                .load(StaticValues.CoverUrl + beat.getCover())
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                .placeholder(R.mipmap.ic_launcher) // can also be a drawable
                .error(R.mipmap.ic_launcher)// will be displayed if the image cannot be loaded
                .centerCrop()
                .crossFade()
                .into(holder.img);


    }

    @Override
    public int getItemCount() {
        return BeatList.size();
    }

    public void add(List<Beat> items) {

        int previousDataSize = this.BeatList.size();

        if (previousDataSize == 0) {
            BeatList.addAll(items);
            notifyItemInserted(1);
        } else {
            BeatList.addAll(items);
            notifyItemRangeInserted(previousDataSize, items.size());
        }


    }

    public void clear() {
        final int size = BeatList.size();
        if (size > 0) {

            BeatList.clear();

            notifyItemRangeRemoved(0, size);
        }
        notifyDataSetChanged();
    }



}

