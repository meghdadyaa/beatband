package com.gilanweb.BeatBand.adapter;


import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.gilanweb.BeatBand.R;
import com.gilanweb.BeatBand.Retrofit.Model.Adv;
import com.gilanweb.BeatBand.Utils.StaticValues;

import java.util.List;


public class AdvAdapter extends RecyclerView.Adapter<AdvAdapter.RecyclerViewHolder> {

    List<Adv> advList;
    Context context;
    View view;
    final Handler handler = new Handler();
    private boolean mScrollable;


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {


        ImageView img;
        CardView cardCatItem;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.beat_image);

        }
    }

    public AdvAdapter(List<Adv> advs , Context context) {
        this.advList = advs;
        this.context = context;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_beat, parent, false);
        RecyclerViewHolder recyclerViewHolder = new RecyclerViewHolder(itemView);
        view = itemView;
        return recyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        Adv adv = advList.get(position);
        Glide
                .with(context)
                .load(StaticValues.AdvUrl + adv.getImage())
                .placeholder(R.mipmap.ic_launcher) // can also be a drawable
                .error(R.mipmap.ic_launcher)// will be displayed if the image cannot be loaded
                .centerCrop()
                .crossFade()
                .into(holder.img);


    }

    @Override
    public int getItemCount() {
        return advList.size();
    }


}

