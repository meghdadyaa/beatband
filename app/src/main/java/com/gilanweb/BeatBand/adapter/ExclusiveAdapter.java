package com.gilanweb.BeatBand.adapter;


import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.Target;
import com.gilanweb.BeatBand.R;
import com.gilanweb.BeatBand.Retrofit.Model.Beat;
import com.gilanweb.BeatBand.Utils.StaticValues;

import java.util.List;


public class ExclusiveAdapter extends RecyclerView.Adapter<ExclusiveAdapter.RecyclerViewHolder> {

    public List<Beat> BeatList;
    Context context;
    View view;

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {


        ImageView img;
        CardView cardCatItem;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.beat_image);


        }
    }

    public ExclusiveAdapter(List<Beat> beats, Context context) {
        this.BeatList = beats;
        this.context = context;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_beat, parent, false);
        RecyclerViewHolder recyclerViewHolder = new RecyclerViewHolder(itemView);
        view = itemView;
        return recyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        Beat beat = BeatList.get(position);
        Glide
                .with(context)
                .load(StaticValues.CoverUrl + beat.getCover())
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                .placeholder(R.mipmap.ic_launcher) // can also be a drawable
                .error(R.mipmap.ic_launcher)// will be displayed if the image cannot be loaded
                .centerCrop()
                .crossFade()
                .into(holder.img);


    }

    @Override
    public int getItemCount() {
        return BeatList.size();
    }

    public void add(List<Beat> items) {

        int previousDataSize = this.BeatList.size();

        if(previousDataSize==0){
            BeatList.addAll(items);
            notifyItemInserted(1);
        }else {
            BeatList.addAll(items);
            notifyItemRangeInserted(previousDataSize, items.size());
        }


    }


}

