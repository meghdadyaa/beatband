package com.gilanweb.BeatBand.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gilanweb.BeatBand.R;
import com.gilanweb.BeatBand.Retrofit.Model.Allprice;
import com.gilanweb.BeatBand.Retrofit.Model.Genre;
import com.gilanweb.BeatBand.Utils.DatePreferences;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class PriceAdapter extends RecyclerView.Adapter<PriceAdapter.RecyclerViewHolder> {


    public List<Allprice> allpriceList;
    View view;
    Context context;
    DatePreferences preferences;

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {


        TextView textView;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.tag_id);


        }
    }


    public PriceAdapter(Context context) {
        allpriceList = new ArrayList<>();
        this.context = context;
        preferences = new DatePreferences(context);
    }

    @Override
    public PriceAdapter.RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_genre, parent, false);
        PriceAdapter.RecyclerViewHolder recyclerViewHolder = new PriceAdapter.RecyclerViewHolder(itemView);
        view = itemView;
        return recyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {
        final Allprice allprice = allpriceList.get(position);
        holder.textView.setText("#" + allprice.getLable());
        holder.textView.setTextColor(allpriceList.get(position).isSelected() ? ContextCompat.getColor(context, R.color.selected_tag_text_color) : ContextCompat.getColor(context, R.color.tag_text_color));
        if (allpriceList.get(position).isSelected()) {
            holder.itemView.setBackground(context.getResources().getDrawable(R.drawable.background_tag_item));
        } else {
            holder.itemView.setBackgroundColor(Color.TRANSPARENT);
        }

        holder.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                for (int k = 0; k < allpriceList.size(); k++) {
                    if (k == position) {
                        allpriceList.get(k).setSelected(true);
                        preferences.SetTagPrice(allprice.getPrice());

                    } else {
                        allpriceList.get(k).setSelected(false);

                    }
                }
                notifyDataSetChanged();

            }
        });


    }


    @Override
    public int getItemCount() {
        return allpriceList.size();
    }

    public void add(List<Allprice> items) {

        int previousDataSize = this.allpriceList.size();

        if (previousDataSize == 0) {
            allpriceList.addAll(items);
            notifyItemInserted(1);
        } else {
            allpriceList.addAll(items);
            notifyItemRangeInserted(previousDataSize, items.size());
        }


    }
}
