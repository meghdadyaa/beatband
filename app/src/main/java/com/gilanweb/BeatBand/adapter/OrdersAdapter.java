package com.gilanweb.BeatBand.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.gilanweb.BeatBand.R;
import com.gilanweb.BeatBand.Retrofit.Model.OrderOrders;
import com.gilanweb.BeatBand.Utils.StaticValues;

import java.util.List;


public class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.RecyclerViewHolder> {

    public List<OrderOrders> orderOrdersList;
    Context context;
    View view;

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {


        ImageView imageView;
        TextView price, code, date;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.poster);
            price = (TextView) itemView.findViewById(R.id.price);
            code = (TextView) itemView.findViewById(R.id.code);
            date = (TextView) itemView.findViewById(R.id.date);

        }
    }

    public OrdersAdapter(List<OrderOrders> beats, Context context) {
        this.orderOrdersList = beats;
        this.context = context;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_order_beat, parent, false);
        RecyclerViewHolder recyclerViewHolder = new RecyclerViewHolder(itemView);
        view = itemView;
        return recyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        OrderOrders orderOrders = orderOrdersList.get(position);
        holder.price.setText(orderOrders.getPrice());
        holder.code.setText(orderOrders.getName());
        holder.date.setText(orderOrders.getShop_date());
        Glide
                .with(context)
                .load(StaticValues.CoverUrl + orderOrders.getBeat_id()+"_cover.jpg")
                .placeholder(R.mipmap.ic_launcher) // can also be a drawable
                .error(R.mipmap.ic_launcher)// will be displayed if the image cannot be loaded
                .centerCrop()
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .into(holder.imageView);


    }

    @Override
    public int getItemCount() {
        return orderOrdersList.size();
    }

    public void add(List<OrderOrders> items) {

        int previousDataSize = this.orderOrdersList.size();

        if(previousDataSize==0){
            orderOrdersList.addAll(items);
            notifyItemInserted(1);
        }else {
            orderOrdersList.addAll(items);
            notifyItemRangeInserted(previousDataSize, items.size());
        }


    }


}

