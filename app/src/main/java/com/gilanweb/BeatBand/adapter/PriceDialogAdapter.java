package com.gilanweb.BeatBand.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gilanweb.BeatBand.R;
import com.gilanweb.BeatBand.Retrofit.Model.Allprice;
import com.gilanweb.BeatBand.Retrofit.Model.ApiClient;
import com.gilanweb.BeatBand.Retrofit.Model.ApiInterface;
import com.gilanweb.BeatBand.Retrofit.Model.CheckExpaire;
import com.gilanweb.BeatBand.Retrofit.Model.Prices;
import com.gilanweb.BeatBand.activity.LoginGoogleActivity;
import com.gilanweb.BeatBand.activity.PreviewActivity;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PriceDialogAdapter extends RecyclerView.Adapter<PriceDialogAdapter.RecyclerViewHolder> {


    public List<Prices> allpriceList;
    View view;
    Context context;

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {


        TextView title;
        TextView descrption;
        TextView price;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title_beat);
            descrption = itemView.findViewById(R.id.description_baet);
            price = itemView.findViewById(R.id.price_beat);

        }
    }


    public PriceDialogAdapter(Context context) {
        allpriceList = new ArrayList<>();
        this.context = context;
    }

    @Override
    public PriceDialogAdapter.RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_price, parent, false);
        PriceDialogAdapter.RecyclerViewHolder recyclerViewHolder = new PriceDialogAdapter.RecyclerViewHolder(itemView);
        view = itemView;
        return recyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {
        final Prices allprice = allpriceList.get(position);
        holder.title.setText(allprice.getTitle());
        holder.descrption.setText(allprice.getDescrption());
        holder.price.setText(allprice.getPrice() + "T");
        holder.title.setTextColor(allpriceList.get(position).isSelected() ? ContextCompat.getColor(context, R.color.selected_price_text_color) : ContextCompat.getColor(context, R.color.tag_text_color));
        holder.descrption.setTextColor(allpriceList.get(position).isSelected() ? ContextCompat.getColor(context, R.color.selected_price_text_color) : ContextCompat.getColor(context, R.color.tag_text_color));
        holder.price.setTextColor(allpriceList.get(position).isSelected() ? ContextCompat.getColor(context, R.color.selected_price_text_color) : ContextCompat.getColor(context, R.color.tag_text_color));
        if (allpriceList.get(position).isSelected()) {
            holder.itemView.setBackground(context.getResources().getDrawable(R.drawable.background_price_item));
        } else {
            holder.itemView.setBackgroundColor(Color.TRANSPARENT);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                for (int k = 0; k < allpriceList.size(); k++) {
                    if (k == position) {
                        allpriceList.get(k).setSelected(true);

                    } else {
                        allpriceList.get(k).setSelected(false);

                    }
                }
                notifyDataSetChanged();

            }
        });


    }

    public Prices getItemSelected() {
        Prices pricese = new Prices();
        for (Prices prices : allpriceList) {
            if (prices.isSelected()) {
                pricese = prices;
            }
        }
        return pricese;
    }




    @Override
    public int getItemCount() {
        return allpriceList.size();
    }

    public void add(List<Prices> items) {

        int previousDataSize = this.allpriceList.size();

        if (previousDataSize == 0) {
            allpriceList.addAll(items);
            notifyItemInserted(1);
        } else {
            allpriceList.addAll(items);
            notifyItemRangeInserted(previousDataSize, items.size());
        }


    }
}
