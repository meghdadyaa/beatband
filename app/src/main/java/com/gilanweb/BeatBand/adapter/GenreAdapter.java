package com.gilanweb.BeatBand.adapter;

import android.app.Activity;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gilanweb.BeatBand.R;
import com.gilanweb.BeatBand.Retrofit.Model.Genre;
import com.gilanweb.BeatBand.Utils.DatePreferences;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import static java.lang.System.out;

public class GenreAdapter extends RecyclerView.Adapter<GenreAdapter.RecyclerViewHolder> {


    public List<Genre> genres;
    View view;
    Activity activity;
    List<Integer> idList = new ArrayList<>();
    DatePreferences preferences;

    public GenreAdapter(Activity activity) {
        genres = new ArrayList<>();
        this.activity = activity;
        preferences = new DatePreferences(activity);
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {


        TextView textView;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.tag_id);


        }
    }

    @Override
    public GenreAdapter.RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_genre, parent, false);
        GenreAdapter.RecyclerViewHolder recyclerViewHolder = new GenreAdapter.RecyclerViewHolder(itemView);
        view = itemView;
        return recyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, int position) {
        final Genre genre = genres.get(position);
        holder.textView.setText("#" + genre.getName());
        holder.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                genre.setSelected(!genre.isSelected());
                if (genre.isSelected()) {
                    holder.itemView.setBackground(activity.getResources().getDrawable(R.drawable.background_tag_item));
                    idList.add(Integer.valueOf(genre.getId()));
                    preferences.setTagIds(new Gson().toJson(idList));
                } else {
                    holder.itemView.setBackgroundColor(Color.TRANSPARENT);
                    idList.remove(Integer.valueOf(genre.getId()));
                    preferences.setTagIds(new Gson().toJson(idList));
                }
                holder.textView.setTextColor(genre.isSelected() ? ContextCompat.getColor(activity, R.color.selected_tag_text_color) : ContextCompat.getColor(activity, R.color.tag_text_color));
            }
        });
    }


    @Override
    public int getItemCount() {
        return genres.size();
    }

    public void add(List<Genre> items) {

        int previousDataSize = this.genres.size();

        if (previousDataSize == 0) {
            genres.addAll(items);
            notifyItemInserted(1);
        } else {
            genres.addAll(items);
            notifyItemRangeInserted(previousDataSize, items.size());
        }


    }
}
