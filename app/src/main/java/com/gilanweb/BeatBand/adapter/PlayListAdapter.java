package com.gilanweb.BeatBand.adapter;


import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gilanweb.BeatBand.R;
import com.gilanweb.BeatBand.Retrofit.Model.Beat;
import com.gilanweb.BeatBand.Utils.Config;
import com.gilanweb.BeatBand.Utils.MyDatabase;
import com.gilanweb.BeatBand.Utils.StaticValues;
import com.gilanweb.BeatBand.Utils.Utils;
import com.gilanweb.BeatBand.activity.NowPlayingActivity;
import com.gilanweb.BeatBand.activity.dialog.removeBeatDialog;
import com.gilanweb.BeatBand.services.BusProvider;
import com.gilanweb.BeatBand.services.Constants;
import com.gilanweb.BeatBand.services.Events;
import com.gilanweb.BeatBand.services.MusicService;
import com.squareup.otto.Subscribe;

import java.util.List;

import static com.gilanweb.BeatBand.Utils.StaticValues.setBeatArrayList;
import static com.gilanweb.BeatBand.Utils.StaticValues.setSongIndex;


public class PlayListAdapter extends RecyclerView.Adapter<PlayListAdapter.RecyclerViewHolder> {

    List<Beat> BeatList;
    Context context;
    View view;
    private static Bitmap mPlayBitmap;
    private static Bitmap mPauseBitmap;
    private static Bitmap repeatBitmap;
    boolean nowPlaying = false;
    int index;
    protected Config mConfig;
    FragmentManager man;


    public PlayListAdapter(List<Beat> beats, Context context, FragmentManager man) {
        this.BeatList = beats;
        this.context = context;
        BusProvider.getInstance().register(this);
        mConfig = Config.newInstance(context);
        this.man = man;
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {


        ImageView img;
        Button buy;
        TextView beatName;
        ImageView play;
        // ImageView repeat;
        ImageView trash;
        TextView remove;
        RelativeLayout item;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.beatlist_image);
            play = itemView.findViewById(R.id.img_play);
            //   repeat = (ImageView) itemView.findViewById(R.id.img_reapet);
            trash = itemView.findViewById(R.id.img_trash);
            buy = itemView.findViewById(R.id.button_buy);
            beatName = itemView.findViewById(R.id.playlist_beat_name);
            remove = itemView.findViewById(R.id.txt_trash);
            item = itemView.findViewById(R.id.item_click);
            //  Blurry.with(context).radius(25).sampling(2).onto((ViewGroup) cardCatItem);
            //  Blurry.with(context).radius(25).sampling(2).onto(cardCatItem);


        }
    }


    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.playlist_item_beat, parent, false);
        RecyclerViewHolder recyclerViewHolder = new RecyclerViewHolder(itemView);
        view = itemView;
        return recyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {
        final Beat beat = BeatList.get(position);
        holder.beatName.setText(beat.getName());
        Glide
                .with(context)
                .load(StaticValues.CoverUrl + beat.getCover())
                .placeholder(R.mipmap.ic_launcher) // can also be a drawable
                .error(R.mipmap.ic_launcher)// will be displayed if the image cannot be loaded
                .crossFade()
                .into(holder.img);
        setupIconColors();

        holder.buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setSongIndex(Long.valueOf(position));
                context.startActivity(new Intent(context, NowPlayingActivity.class).putExtra("class", "PlayListActivity"));
            }
        });
        holder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setSongIndex(Long.valueOf(position));
                context.startActivity(new Intent(context, NowPlayingActivity.class).putExtra("class", "PlayListActivity"));
            }
        });

        if (beat.getSold().equals("1")) {
            holder.buy.setVisibility(View.GONE);

        } else {
            holder.buy.setVisibility(View.VISIBLE);
        }

        holder.play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                index = position;
                if (StaticValues.getmCurrentSong() == null) {
                    setBeatArrayList(BeatList);
                    songPicked(position);
                    StaticValues.setmCurrentSong(beat);


                } else {
                    if (StaticValues.getmCurrentSong().getId().equals(beat.getId())) {
                        Utils.sendIntent(context, Constants.PLAYPAUSE);
                    } else {
                        setBeatArrayList(BeatList);
                        songPicked(position);
                        StaticValues.setmCurrentSong(beat);
                    }
                }
            }
        });

        if (StaticValues.getmCurrentSong() != null)
            if (StaticValues.getmCurrentSong().getId().equals(beat.getId())) {
                if (nowPlaying) {
                    holder.play.setImageBitmap(mPauseBitmap);
                } else {
                    holder.play.setImageBitmap(mPlayBitmap);
                }
            } else {

                holder.play.setImageBitmap(mPlayBitmap);
            }


        holder.buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                if (beat.getSold().equals("1")) {
//
//
//                } else {
//                    context.startActivity(new Intent(context, PreviewActivity.class).putExtra("code", beat.getId()).putExtra("price", beat.getWave_price()).putExtra("date", beat.getCretaed_at()).putExtra("img", beat.getCover()).putExtra("fileprice", beat.getFile_price()));
//                }


            }
        });


     /*   holder.repeat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mConfig.getRepeatSong() == true) {
                    toggleSongRepetition(false);
                    holder.repeat.setImageResource(R.drawable.not_ret);

                } else {
                    toggleSongRepetition(true);
                    holder.repeat.setImageBitmap(repeatBitmap);
                }

            }
        });*/
        holder.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog(position);
            }
        });
        holder.trash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                notifyItemRangeRemoved(position, getItemCount());
//                new MyDatabase(context).deletebeat(beat.getId());
//                BeatList.remove(position);

                showDialog(position);


            }
        });

        holder.buy.setText("$ BUY     " + beat.getWave_price() + "T");


    }

    private void toggleSongRepetition(boolean enable) {
        mConfig.setRepeatSong(enable);
    }

    private void songPicked(int pos) {
        final Intent intent = new Intent(context, MusicService.class);
        intent.putExtra(Constants.SONG_POS, pos);
        intent.setAction(Constants.PLAYPOS);
        context.startService(intent);
    }

    @Override
    public int getItemCount() {
        return BeatList.size();
    }

    @Subscribe
    public void songChangedEvent(Events.SongChanged event) {
        StaticValues.setmCurrentSong(event.getSong());
        notifyDataSetChanged();

    }

    @Subscribe
    public void songStateChanged(Events.SongStateChanged event) {

        if (event.getIsPlaying()) {
            nowPlaying = true;
            notifyDataSetChanged();
        } else {
            nowPlaying = false;
            notifyDataSetChanged();
        }
    }

    public void removeat(int pos) {
        notifyItemRangeRemoved(pos, getItemCount());
        notifyItemChanged(pos);
        new MyDatabase(context).deletebeat(BeatList.get(pos).getId());
        BeatList.remove(pos);
    }


    private void setupIconColors() {
        final Resources res = context.getResources();
        final int color = Color.WHITE;
        mPlayBitmap = Utils.getColoredIcon(res, color, R.drawable.play_list);
        mPauseBitmap = Utils.getColoredIcon(res, color, R.drawable.pause_list);
        //  repeatBitmap = Utils.getColoredIcon(res, color, R.drawable.not_ret);
    }


    public void showDialog(int Postion) {
        FragmentTransaction ft = man.beginTransaction();
        Fragment prev = man.findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        DialogFragment dialogFragment = new removeBeatDialog();
        Bundle bundle = new Bundle();
        bundle.putInt("pos", Postion);
        dialogFragment.setArguments(bundle);
        dialogFragment.show(ft, "dialog");
    }

}

