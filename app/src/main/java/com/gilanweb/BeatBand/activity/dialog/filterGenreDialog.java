package com.gilanweb.BeatBand.activity.dialog;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;

import com.gilanweb.BeatBand.R;
import com.gilanweb.BeatBand.Retrofit.Model.Allprice;
import com.gilanweb.BeatBand.Retrofit.Model.Genre;
import com.gilanweb.BeatBand.Retrofit.Time;
import com.gilanweb.BeatBand.Utils.DatePreferences;
import com.gilanweb.BeatBand.adapter.GenreAdapter;
import com.gilanweb.BeatBand.adapter.PriceAdapter;
import com.gilanweb.BeatBand.adapter.TimeAdapter;
import com.google.android.flexbox.AlignItems;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexWrap;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

public class filterGenreDialog extends DialogFragment {


    private setFilter clickDialog;
    RecyclerView genreRecyclerView;
    RecyclerView priceRecyclerView;
    RecyclerView timeRecyclerView;
    LinearLayout linearLayout;
    LinearLayoutManager genreLinearLayoutManager;
    LinearLayoutManager priceLinearLayoutManager;
    LinearLayoutManager timeLinearLayoutManager;
    GenreAdapter genreAdapter;
    PriceAdapter priceAdapter;
    TimeAdapter timeAdapter;
    Gson gson = new Gson();
    Button btnDefault;
    Button btnSet;
    DatePreferences preferences;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_FRAME, android.R.style.Theme_Dialog);
    }

    @Override
    public void onAttach(Activity ac) {
        super.onAttach(ac);
        clickDialog = (setFilter) ac;
    }

    @Override
    public void onDetach() {
        clickDialog = null;
        super.onDetach();
    }

    @Override
    public void onStart() {
        super.onStart();
        Window window = getDialog().getWindow();
        WindowManager.LayoutParams params = window.getAttributes();
        params.dimAmount = 0.6f;
        window.setAttributes(params);
        window.setBackgroundDrawableResource(android.R.color.transparent);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_genre_filter, container, false);
        linearLayout = view.findViewById(R.id.container_body);
        genreRecyclerView = view.findViewById(R.id.rec_genre);
        priceRecyclerView = view.findViewById(R.id.rec_price);
        timeRecyclerView = view.findViewById(R.id.rec_time);
        btnDefault = view.findViewById(R.id.btn_default);
        btnSet = view.findViewById(R.id.btn_set);
        preferences = new DatePreferences(getContext());
        btnSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickDialog.genreFilter();
                dismiss();
            }
        });
        btnDefault.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                preferences.SetTagTime("new");
                preferences.setTagIds("0");
                preferences.SetTagPrice("0");
                clickDialog.genreFilter();
                dismiss();
            }
        });
        init();
        return view;
    }


    public void init() {
        FlexboxLayoutManager genreLinearLayoutManager = new FlexboxLayoutManager(getActivity());
        genreLinearLayoutManager.setFlexDirection(FlexDirection.ROW);
        genreLinearLayoutManager.setJustifyContent(JustifyContent.FLEX_START);
        genreLinearLayoutManager.setFlexWrap(FlexWrap.WRAP);
        genreLinearLayoutManager.setAlignItems(AlignItems.FLEX_START);
        FlexboxLayoutManager priceLinearLayoutManager = new FlexboxLayoutManager(getActivity());
        priceLinearLayoutManager.setFlexDirection(FlexDirection.ROW);
        priceLinearLayoutManager.setJustifyContent(JustifyContent.FLEX_START);
        priceLinearLayoutManager.setFlexWrap(FlexWrap.WRAP);
        priceLinearLayoutManager.setAlignItems(AlignItems.FLEX_START);
        FlexboxLayoutManager timeLinearLayoutManager = new FlexboxLayoutManager(getActivity());
        timeLinearLayoutManager.setFlexDirection(FlexDirection.ROW);
        timeLinearLayoutManager.setJustifyContent(JustifyContent.FLEX_START);
        timeLinearLayoutManager.setFlexWrap(FlexWrap.WRAP);
        timeLinearLayoutManager.setAlignItems(AlignItems.FLEX_START);
        genreRecyclerView.setLayoutManager(genreLinearLayoutManager);
        priceRecyclerView.setLayoutManager(priceLinearLayoutManager);
        timeRecyclerView.setLayoutManager(timeLinearLayoutManager);
        genreAdapter = new GenreAdapter(getActivity());
        priceAdapter = new PriceAdapter(getActivity());
        timeAdapter = new TimeAdapter(getActivity());
        genreRecyclerView.setAdapter(genreAdapter);
        priceRecyclerView.setAdapter(priceAdapter);
        timeRecyclerView.setAdapter(timeAdapter);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        linearLayout.getLayoutParams().height = height / 2;
        Bundle bundle = getArguments();
        String genre = bundle.getString("genre", "");
        String price = bundle.getString("price", "");
        List<Genre> genres = gson.fromJson(genre, new TypeToken<List<Genre>>() {
        }.getType());
        genreAdapter.add(genres);
        List<Allprice> allprices = gson.fromJson(price, new TypeToken<List<Allprice>>() {
        }.getType());
        priceAdapter.add(allprices);
        Time time = new Time();
        time.setTime("new");
        Time time1 = new Time();
        time1.setTime("old");
        List<Time> times = new ArrayList<>();
        times.add(time);
        times.add(time1);
        timeAdapter.add(times);
        setDialogPosition();
    }

    public interface setFilter {
         void genreFilter();
    }


    private void setDialogPosition() {
        Window window = getDialog().getWindow();
        window.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);

        WindowManager.LayoutParams params = window.getAttributes();
        params.y = dpToPx(60);
        window.setAttributes(params);
    }

    private int dpToPx(int dp) {
        DisplayMetrics metrics = getActivity().getResources().getDisplayMetrics();
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, metrics);
    }
}