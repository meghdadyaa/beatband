package com.gilanweb.BeatBand.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gilanweb.BeatBand.R;
import com.gilanweb.BeatBand.Retrofit.Model.ApiClient;
import com.gilanweb.BeatBand.Retrofit.Model.ApiInterface;
import com.gilanweb.BeatBand.Retrofit.Model.CheckExpaire;
import com.gilanweb.BeatBand.Retrofit.Model.LoginResponse;
import com.gilanweb.BeatBand.Utils.Config;
import com.gilanweb.BeatBand.Utils.StaticValues;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PreviewActivity extends AppCompatActivity {

    ImageView imageView;
    TextView  code, date;
    private static Config mConfig;
    RadioButton price, fileprice;
    String choose="2";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview);
        mConfig = Config.newInstance(getApplicationContext());
        Toolbar mToolbar = (Toolbar) findViewById(R.id.now_playing_toolbar);
     /*   setSupportActionBar(mToolbar);
        mToolbar.setNavigationIcon(R.drawable.ic_close_white_24dp);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });*/
        init();


    }


    void init() {
        imageView = findViewById(R.id.poster);
        price = findViewById(R.id.price1);
        fileprice= findViewById(R.id.price2);
        code = findViewById(R.id.code);
        date = findViewById(R.id.date);
        price.setText(getIntent().getExtras().getString("price")+" t "+"(wave file)");
        fileprice.setText(getIntent().getExtras().getString("fileprice")+" t "+"(project file)");
        code.setText(getIntent().getExtras().getString("code"));
        date.setText(getIntent().getExtras().getString("date"));
        Glide
                .with(this)
                .load(StaticValues.CoverUrl + getIntent().getExtras().getString("img"))
                .placeholder(R.mipmap.ic_launcher) // can also be a drawable
                .error(R.mipmap.ic_launcher)// will be displayed if the image cannot be loaded
                .centerCrop()
                .crossFade()
                .into(imageView);
        RadioGroup rg = findViewById(R.id.radioGroup1);

        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case R.id.price1:
                        // do operations specific to this selection
                        choose="1";
                        break;
                    case R.id.price2:
                        // do operations specific to this selection
                        choose="2";
                        break;
                }
            }
        });
    }





    public void buy_onclick(View view) {

        if (!mConfig.getApiKey().equals("")) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<CheckExpaire> postlogin = apiService.logincheck(mConfig.getApiKey());
        postlogin.enqueue(new Callback<CheckExpaire>() {
            @Override
            public void onResponse(Call<CheckExpaire> call, Response<CheckExpaire> response) {

                if(response.body().getLoginexpire().equals("no-expire")){
                    Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("http://mybeatband.com/dashboard/pay/" + mConfig.getApiKey() + "/" + getIntent().getExtras().getString("code") + "/"+choose));
                    startActivity(i);
                    finish();

                }else {
                    startActivity(new Intent(PreviewActivity.this, LoginGoogleActivity.class).putExtra("logout", true));
                }

            }

            @Override
            public void onFailure(Call<CheckExpaire> call, Throwable t) {

            }
        });

        } else {
            startActivity(new Intent(PreviewActivity.this, LoginGoogleActivity.class));
        }


    }
}
