package com.gilanweb.BeatBand.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.gilanweb.BeatBand.R;
import com.gilanweb.BeatBand.Utils.Config;
import com.gilanweb.BeatBand.Utils.MyDatabase;
import com.gilanweb.BeatBand.activity.dialog.removeBeatDialog;
import com.gilanweb.BeatBand.adapter.PlayListAdapter;
import com.gilanweb.BeatBand.services.BusProvider;
import com.gilanweb.BeatBand.services.Constants;
import com.gilanweb.BeatBand.services.MusicService;
import com.squareup.otto.Bus;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class PlayListActivity extends AppCompatActivity implements FragmentDrawer.FragmentDrawerListener, removeBeatDialog.removebtn {
    Toolbar mToolbar;
    RecyclerView recyclerView;
    PlayListAdapter mAdapter;
    private static Bus mBus;
    protected Config mConfig;
    FragmentDrawer drawerFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Montserrat-Medium.otf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        setContentView(R.layout.activity_play_list_activity);
        mToolbar = findViewById(R.id.now_playing_toolbar);
        //  Blurry.with(this).radius(25).sampling(2).onto(mToolbar);
        recyclerView = findViewById(R.id.playlist_beat);
        mBus = BusProvider.getInstance();
        mBus.register(PlayListActivity.this);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        setTitle(getResources().getString(R.string.app_name));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

            }
        });
        Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.mipmap.ic_navigation_drawer, getTheme());
        getSupportActionBar().setHomeAsUpIndicator(drawable);
        drawerFragment = (FragmentDrawer)
                getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), mToolbar);
        drawerFragment.setDrawerListener(this);

        DividerItemDecoration itemDecorator = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(this, R.drawable.divider));
        mAdapter = new PlayListAdapter(new MyDatabase(this).getAllPlayList(), PlayListActivity.this, getSupportFragmentManager());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(PlayListActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(mAdapter);
        recyclerView.setItemAnimator(null);
        recyclerView.addItemDecoration(itemDecorator);


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopMusic();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.playlist) {
            startActivity(new Intent(PlayListActivity.this, PlayListActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public void stopMusic() {
        final Intent intent = new Intent(PlayListActivity.this, MusicService.class);
        intent.setAction(Constants.FINISH);
        startService(intent);
    }


    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);
    }

    private void displayView(int position) {
        Fragment fragment = null;
        String title = getString(R.string.app_name);
        switch (position) {
            case 0:

                break;
            case 1:
                startActivity(new Intent(PlayListActivity.this, PlayListActivity.class));
                break;
            case 2:
                if (mConfig.getApiKey().equals("")) {
                    startActivity(new Intent(PlayListActivity.this, LoginGoogleActivity.class));
                } else {
                    startActivity(new Intent(PlayListActivity.this, RecentMusicActivity.class));
                }
                break;
            case 3:

                break;
            case 4:
                startActivity(new Intent(PlayListActivity.this, HelpActivity.class));
                break;
            case 5:
                startActivity(new Intent(PlayListActivity.this, SettingsActivity.class));
                break;
            case 6:
                startActivity(new Intent(PlayListActivity.this, LoginGoogleActivity.class).putExtra("logout", true));

                break;
            default:
                break;
        }


    }

    @Override
    public void remove(int pos) {
        mAdapter.removeat(pos);
        mAdapter.notifyDataSetChanged();
    }


}
