package com.gilanweb.BeatBand.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.gilanweb.BeatBand.R;
import com.gilanweb.BeatBand.Retrofit.Model.ApiClient;
import com.gilanweb.BeatBand.Retrofit.Model.ApiInterface;
import com.gilanweb.BeatBand.Retrofit.Model.Status;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class RegisterActivity extends AppCompatActivity {
    EditText name;
    EditText email;
    EditText password;
    EditText mobile;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Montserrat-Medium.otf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        setContentView(R.layout.activity_register);
        name = (EditText) findViewById(R.id.name);
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        mobile = (EditText) findViewById(R.id.mobile);
        toolbar = (Toolbar) findViewById(R.id.toolbar_register);

        toolbar.setNavigationIcon(R.drawable.ic_close_white_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });


    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    public void register(View view) {

        if (!name.getText().toString().equals("") && !email.getText().toString().equals("") && !password.getText().toString().equals("") && !mobile.getText().toString().equals("")){
            postRegister();
        }else {
            Toast.makeText(this, "please fill all field", Toast.LENGTH_SHORT).show();
        }
            

    }

    public void postRegister() {

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<Status> postRegister = apiService.postRegister(name.getText().toString(), email.getText().toString(), password.getText().toString(), mobile.getText().toString());
        postRegister.enqueue(new Callback<Status>() {
            @Override
            public void onResponse(Call<Status> call, Response<Status> response) {
                try {

                    if (response.body().getStatus().equals("confrim email")) {

                        startActivity(new Intent(RegisterActivity.this, thanksActivity.class));

                    } else if(response.body().getStatus().equals("tekrari")) {
                        Toast.makeText(RegisterActivity.this, "try with new email address", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }

            }

            @Override
            public void onFailure(Call<Status> call, Throwable t) {

            }
        });
    }

}
