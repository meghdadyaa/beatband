package com.gilanweb.BeatBand.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.gilanweb.BeatBand.R;
import com.gilanweb.BeatBand.Retrofit.Model.ApiClient;
import com.gilanweb.BeatBand.Retrofit.Model.ApiInterface;
import com.gilanweb.BeatBand.Retrofit.Model.Getversion;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class SettingsActivity extends AppCompatActivity {
    String link;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Montserrat-Medium.otf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        setContentView(R.layout.activity_settings);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_setting);
        toolbar.setNavigationIcon(R.drawable.ic_close_white_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });


    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    public void ChangeـPassword(View view) {
        String x = "http://gilan3d.ir/music/public/password/reset";
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(x));
        startActivity(intent);

    }

    public void checkUpdate(View view) {
        postVersion(getVersion());
    }


    String getVersion() {
        String version = "1";
        try {
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return version;
    }


    void postVersion(String version) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<Getversion> getversionCall= apiService.getVersion(getVersion());
        getversionCall.enqueue(new Callback<Getversion>() {
            @Override
            public void onResponse(Call<Getversion> call, Response<Getversion> response) {
                if(!response.body().getLink().isEmpty())
                    link = response.body().getLink();
                    startDownloading(response.body().getLink());
            }

            @Override
            public void onFailure(Call<Getversion> call, Throwable t) {
                TextView upNote = findViewById(R.id.updateNote);
                upNote.setVisibility(View.VISIBLE);
            }
        });

    }

    void startDownloading(String link) {

        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link.toString()));
        startActivity(browserIntent);
    }

}
