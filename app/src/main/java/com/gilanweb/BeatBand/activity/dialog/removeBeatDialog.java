package com.gilanweb.BeatBand.activity.dialog;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.gilanweb.BeatBand.R;


public class removeBeatDialog extends DialogFragment {

    private removebtn clickDialog;

    public interface removebtn {
        void remove(int postion);
    }

    @Override
    public void onAttach(Activity ac) {
        super.onAttach(ac);
        clickDialog = (removebtn) ac;
    }

    @Override
    public void onDetach() {
        clickDialog = null;
        super.onDetach();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_FRAME, android.R.style.Theme_Dialog);
    }

    @Override
    public void onStart() {
        super.onStart();
        Window window = getDialog().getWindow();
        WindowManager.LayoutParams params = window.getAttributes();
        params.dimAmount = 0.6f;
        window.setAttributes(params);
        window.setBackgroundDrawableResource(android.R.color.transparent);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_beat_remove, container, false);
        Button btnNo = view.findViewById(R.id.btn_no);
        Button btnYes = view.findViewById(R.id.btn_yes);
        Bundle bundle = getArguments();
        final int pos = bundle.getInt("pos", 0);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((removebtn) getContext()).remove(pos);
                dismiss();
            }
        });

        init();
        setDialogPosition();
        return view;
    }


    public void init() {

    }


    private void setDialogPosition() {
        Window window = getDialog().getWindow();
        window.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);

        WindowManager.LayoutParams params = window.getAttributes();
        params.y = dpToPx(60);
        window.setAttributes(params);
    }

    private int dpToPx(int dp) {
        DisplayMetrics metrics = getActivity().getResources().getDisplayMetrics();
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, metrics);
    }

}
