package com.gilanweb.BeatBand.activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.gilanweb.BeatBand.R;
import com.gilanweb.BeatBand.Retrofit.Model.ApiClient;
import com.gilanweb.BeatBand.Retrofit.Model.ApiInterface;
import com.gilanweb.BeatBand.Retrofit.Model.LoginResponse;
import com.gilanweb.BeatBand.Utils.Config;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class LoginActivity extends AppCompatActivity {

    EditText email;
    EditText password;
    private static Config mConfig;
    Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Montserrat-Medium.otf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        setContentView(R.layout.activity_login_simple);
        mConfig = Config.newInstance(getApplicationContext());
        email = findViewById(R.id.email_login);
        password = findViewById(R.id.password_login);
        toolbar = findViewById(R.id.toolbar_loginsimple);
        toolbar.setNavigationIcon(R.drawable.ic_close_white_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent goToMainActivity = new Intent(getApplicationContext(), MainActivity.class);
                goToMainActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(goToMainActivity);
            }
        });

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    public void sign_in(View view) {

        if (!email.getText().toString().equals("") && !password.getText().toString().equals(""))
            postLogin();
    }


    public void postLogin() {

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<LoginResponse> postlogin = apiService.postlogin(email.getText().toString(), password.getText().toString());
        postlogin.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

                try {
                    if (!response.body().getLogin_token().equals("")) {
                        mConfig.setApiKey(response.body().getLogin_token());
                        startActivity(new Intent(LoginActivity.this, RecentMusicActivity.class));
                    } else {
                        Toast.makeText(LoginActivity.this, "Problem in server ", Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    // System.out.println(e.getMessage());
                }

            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {

            }
        });
    }

    public void onClickForget(View view) {

        String x = "http://gilan3d.ir/music/public/password/reset";
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(x));
        startActivity(intent);

    }
}
