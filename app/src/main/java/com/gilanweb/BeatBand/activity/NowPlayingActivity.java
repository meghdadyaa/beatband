package com.gilanweb.BeatBand.activity;


import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.gilanweb.BeatBand.R;
import com.gilanweb.BeatBand.Retrofit.Model.ApiClient;
import com.gilanweb.BeatBand.Retrofit.Model.ApiInterface;
import com.gilanweb.BeatBand.Retrofit.Model.Beat;
import com.gilanweb.BeatBand.Retrofit.Model.Recommended;
import com.gilanweb.BeatBand.Utils.Config;
import com.gilanweb.BeatBand.Utils.MyDatabase;
import com.gilanweb.BeatBand.Utils.RecyclerTouchListener;
import com.gilanweb.BeatBand.Utils.StaticValues;
import com.gilanweb.BeatBand.Utils.Utils;
import com.gilanweb.BeatBand.activity.dialog.buyBeatDialog;
import com.gilanweb.BeatBand.adapter.ExclusiveAdapter;
import com.gilanweb.BeatBand.services.BusProvider;
import com.gilanweb.BeatBand.services.Constants;
import com.gilanweb.BeatBand.services.Events;
import com.gilanweb.BeatBand.services.MusicService;
import com.google.gson.Gson;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.gilanweb.BeatBand.Utils.StaticValues.getBeatArrayList;
import static com.gilanweb.BeatBand.Utils.StaticValues.getSongIndex;
import static com.gilanweb.BeatBand.Utils.StaticValues.setBeatArrayList;
import static com.gilanweb.BeatBand.Utils.StaticValues.setSongIndex;

/**
 * Created by meghdadya on 12/31/16.
 */

public class NowPlayingActivity extends AppCompatActivity implements SeekBar.OnSeekBarChangeListener, View.OnClickListener, FragmentDrawer.FragmentDrawerListener {
    Toolbar mToolbar;
    ImageView playButton;
    ImageView playLast;
    ImageView playNext;
    ImageView repeat;
    ImageView add;
    TextView beatDescription;
    TextView titleDsc;
    SeekBar mProgressBar;
    private static Bus mBus;
    private static boolean mIsNumericProgressShown;
    private static Bitmap mPlayBitmap;
    private static Bitmap mPauseBitmap;
    private static Bitmap addToplayList;
    private static Bitmap removefromplayList;
    private static Bitmap repeatBitmap;

    ExclusiveAdapter exclusiveAdapter;
    RecyclerView advRecyclerView;
    protected Config mConfig;
    FragmentDrawer drawerFragment;
    TextView mProgress;
    TextView price;
    Button buyButton;
    ImageView collapsinImage;
    RelativeLayout RelativeLayoutPrice;
    List<Beat> RecommendedBeat = new ArrayList<>();

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (getIntent().getStringExtra("class").equals("PlayListActivity")) {

        } else {
            stopMusic();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        Utils.sendIntent(this, Constants.PAUSE);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Montserrat-Medium.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        setContentView(R.layout.activity_now_playing);

        mToolbar = findViewById(R.id.now_playing_toolbar);
        setSupportActionBar(mToolbar);

        mToolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        setTitle(getResources().getString(R.string.app_name));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

            }
        });
        Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.mipmap.ic_navigation_drawer, getTheme());
        getSupportActionBar().setHomeAsUpIndicator(drawable);
        drawerFragment = (FragmentDrawer)
                getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), mToolbar);
        drawerFragment.setDrawerListener(this);


        RelativeLayoutPrice = findViewById(R.id.relativelayout_price);
        advRecyclerView = findViewById(R.id.adv_rec);

        mProgress = findViewById(R.id.text_view_progress);
        mProgressBar = findViewById(R.id.seek_bar);
        collapsinImage = findViewById(R.id.backdrop);
        mBus = BusProvider.getInstance();
        mBus.register(this);
        mConfig = Config.newInstance(this);
        mProgressBar.setOnSeekBarChangeListener(this);
        playButton = findViewById(R.id.button_play_toggle);
        playButton.setOnClickListener(this);
        playLast = findViewById(R.id.button_play_last);
        playLast.setOnClickListener(this);
        playNext = findViewById(R.id.button_play_next);
        playNext.setOnClickListener(this);
        add = findViewById(R.id.add_to_playlist);
        repeat = findViewById(R.id.button_repeat);
        add.setOnClickListener(this);
        repeat.setOnClickListener(this);
        buyButton = findViewById(R.id.buy_bottom);
        buyButton.setOnClickListener(this);
        price = findViewById(R.id.tv_price);
        beatDescription = findViewById(R.id.beat_description);
        RelativeLayoutPrice.setOnClickListener(this);
        titleDsc = findViewById(R.id.title_dsc);
        if (StaticValues.getmCurrentSong() == null) {
            songPicked(getSongIndex().intValue());

        } else {
            if (!StaticValues.getmCurrentSong().getId().equals(getBeatArrayList().get(getSongIndex().intValue()).getId())) {
                songPicked(getSongIndex().intValue());
            } else {
                updateSongInfo(getBeatArrayList().get(getSongIndex().intValue()));

            }
        }
        exclusiveAdapter = new ExclusiveAdapter(RecommendedBeat, this);

        advRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        advRecyclerView.setAdapter(exclusiveAdapter);
        advRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(this, advRecyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {

                setBeatArrayList(exclusiveAdapter.BeatList);
                setSongIndex(Long.valueOf(position));
                startActivity(new Intent(NowPlayingActivity.this, NowPlayingActivity.class).putExtra("class", "HomeFragment"));
                finish();

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));


    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_singlemusic, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.playlist) {
            startActivity(new Intent(NowPlayingActivity.this, PlayListActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }*/


    private void songPicked(int pos) {
        final Intent intent = new Intent(this, MusicService.class);
        intent.putExtra(Constants.SONG_POS, pos);
        intent.setAction(Constants.PLAYPOS);
        startService(intent);
    }

    public void stopMusic() {
        final Intent intent = new Intent(NowPlayingActivity.this, MusicService.class);
        intent.setAction(Constants.FINISH);
        startService(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        setupIconColors();

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.button_play_toggle:
                Utils.sendIntent(this, Constants.PLAYPAUSE);
                break;
            case R.id.button_play_last:
                Utils.sendIntent(this, Constants.PREVIOUS);
                break;
            case R.id.button_play_next:
                Utils.sendIntent(this, Constants.NEXT);
                break;
            case R.id.add_to_playlist:

                if (new MyDatabase(this).selectBeat(StaticValues.mCurrentSong.getId())) {
                    new MyDatabase(this).deletebeat(StaticValues.mCurrentSong.getId());
                    add.setImageBitmap(addToplayList);
                } else {
                    add.setImageBitmap(removefromplayList);
                    new MyDatabase(this).insertBeat(StaticValues.mCurrentSong);
                }

                break;
            case R.id.button_repeat:
                if (mConfig.getRepeatSong() == true) {
                    toggleSongRepetition(false);
                    repeat.setImageResource(R.drawable.not_ret);

                } else {
                    toggleSongRepetition(true);
                    repeat.setImageBitmap(repeatBitmap);
                }
                break;
            case R.id.relativelayout_price:
                Buy();
                break;

            default:
                break;
        }
    }

    private void setupIconColors() {
        final Resources res = getResources();
        final int color = Color.WHITE;
        mPlayBitmap = Utils.getColoredIcon(res, color, R.drawable.play);
        mPauseBitmap = Utils.getColoredIcon(res, color, R.drawable.pause);
        addToplayList = Utils.getColoredIcon(res, color, R.drawable.add);
        removefromplayList = Utils.getColoredIcon(res, color, R.drawable.remove);
        repeatBitmap = Utils.getColoredIcon(res, color, R.drawable.not_ret);
    }


    private void updateSongInfo(Beat beat) {
        if (beat != null) {
            mProgressBar.setMax(Integer.parseInt(beat.getDuration()));
            mProgressBar.setProgress(0);
            price.setText(beat.getWave_price() + "t");
            playButton.setImageBitmap(mPauseBitmap);
            //  beatDescription.setText("");
            if (new MyDatabase(this).selectBeat(StaticValues.mCurrentSong.getId())) {
                add.setImageBitmap(removefromplayList);
            } else {
                add.setImageBitmap(addToplayList);
            }
            if (mConfig.getRepeatSong()) {

                repeat.setImageBitmap(repeatBitmap);
            } else {
                repeat.setImageResource(R.drawable.not_ret);
            }
            if (beat.getSold().equals("1")) {
                RelativeLayoutPrice.setVisibility(View.GONE);
            } else {
                RelativeLayoutPrice.setVisibility(View.VISIBLE);
            }


            try {
                Glide.with(this).load(StaticValues.CoverUrl + beat.getCover()).asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE).override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL).into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                        Drawable drawable = new BitmapDrawable(resource);
                        collapsinImage.setImageDrawable(drawable);
//                        BitmapDrawable drawable2 = new BitmapDrawable(getResources(), Utils.createBitmap_ScriptIntrinsicBlur(NowPlayingActivity.this, resource, 50));
//                        coordinatorLayout.setBackground(drawable2);
                    }
                });
            } catch (Exception e) {

            }

            beatDescription.setVisibility(View.GONE);
            titleDsc.setVisibility(View.GONE);
        }
        try {
            String genres = new Gson().toJson(beat.getCate());
            getRecommendedList(genres);
            System.out.println(genres);
        } catch (Exception e) {
            System.out.println(e.fillInStackTrace());
        }

    }

    private void toggleSongRepetition(boolean enable) {
        mConfig.setRepeatSong(enable);
    }

    @Subscribe
    public void songChangedEvent(Events.SongChanged event) {
        StaticValues.setmCurrentSong(event.getSong());
        updateSongInfo(StaticValues.getmCurrentSong());


    }

    @Subscribe
    public void songStateChanged(Events.SongStateChanged event) {
        System.out.println(event.getIsPlaying());
        if (event.getIsPlaying()) {
            playButton.setImageBitmap(mPauseBitmap);
        } else {
            playButton.setImageBitmap(mPlayBitmap);
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {

        if (mIsNumericProgressShown) {
            final String duration = Utils.getTimeString(mProgressBar.getMax());
            final String formattedProgress = Utils.getTimeString(progress);

            final String progressText = String.format(getResources().getString(R.string.progress), formattedProgress, duration);
            mProgress.setText(progressText);

        }

    }

    @Subscribe
    public void songChangedEvent(Events.ProgressUpdated event) {
        mProgressBar.setProgress(event.getProgress());


    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        final Intent intent = new Intent(this, MusicService.class);
        intent.putExtra(Constants.PROGRESS, seekBar.getProgress());
        intent.setAction(Constants.SET_PROGRESS);
        startService(intent);

    }


    void Buy() {
        try {
            Beat beat = StaticValues.getmCurrentSong();
            if (beat.getSold().equals("1")) {


            } else {

                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
                if (prev != null) {
                    ft.remove(prev);
                }
                ft.addToBackStack(null);
                DialogFragment dialogFragment = new buyBeatDialog();
                Bundle bundle = new Bundle();
                bundle.putString("file_price", beat.getFile_price());
                bundle.putString("layer_price", beat.getLayer_price());
                bundle.putString("wave_price", beat.getWave_price());
                bundle.putString("image", beat.getCover());
                bundle.putString("beat_name", beat.getName());
                bundle.putString("beat_code", beat.getId());
                dialogFragment.setArguments(bundle);
                dialogFragment.show(ft, "dialog");


            }

        } catch (Exception e) {
            e.fillInStackTrace();
        }

    }

    public void getRecommendedList(String genres) {


        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<Recommended> getAllCall = apiService.getRecommended(genres);
        getAllCall.enqueue(new Callback<Recommended>() {


            @Override
            public void onResponse(Call<Recommended> call, final Response<Recommended> response) {

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (response.body().getRecomended() != null)
                                if (!response.body().getRecomended().isEmpty()) {
                                    exclusiveAdapter.add(response.body().getRecomended());
                                    RecommendedBeat = response.body().getRecomended();
                                }
                        } catch (Exception e) {
                            //  Toast.makeText(NowPlayingActivity.this, "there is no item with this filter", Toast.LENGTH_SHORT).show();
                        }


                    }
                }, 2000);

            }

            @Override
            public void onFailure(Call<Recommended> call, Throwable t) {

                System.out.println(t.fillInStackTrace());

            }
        });


    }


    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);
    }

    private void displayView(int position) {
        switch (position) {
            case 0:

                break;
            case 1:
                startActivity(new Intent(NowPlayingActivity.this, PlayListActivity.class));
                break;
            case 2:
                if (mConfig.getApiKey().equals("")) {
                    startActivity(new Intent(NowPlayingActivity.this, LoginGoogleActivity.class));
                } else {
                    startActivity(new Intent(NowPlayingActivity.this, RecentMusicActivity.class));
                }
                break;
            case 3:

                break;
            case 4:
                startActivity(new Intent(NowPlayingActivity.this, HelpActivity.class));
                break;
            case 5:
                startActivity(new Intent(NowPlayingActivity.this, SettingsActivity.class));
                break;
            case 6:
                startActivity(new Intent(NowPlayingActivity.this, LoginGoogleActivity.class).putExtra("logout", true));

                break;
            default:
                break;
        }


    }
}



