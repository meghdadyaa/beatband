package com.gilanweb.BeatBand.activity;


import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.gilanweb.BeatBand.R;
import com.gilanweb.BeatBand.Retrofit.Model.ApiClient;
import com.gilanweb.BeatBand.Retrofit.Model.ApiInterface;
import com.gilanweb.BeatBand.Utils.Config;
import com.gilanweb.BeatBand.Utils.MessageEvent;
import com.gilanweb.BeatBand.Utils.MyDatabase;
import com.gilanweb.BeatBand.Utils.Utils;
import com.gilanweb.BeatBand.activity.dialog.filterGenreDialog;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity implements FragmentDrawer.FragmentDrawerListener, filterGenreDialog.setFilter {

    private static String tag = MainActivity.class.getSimpleName();

    Toolbar mToolbar;
    FragmentDrawer drawerFragment;
    boolean doubleBackToExitPressedOnce = false;
    LinearLayout linearLayout;
    private static Config mConfig;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mConfig = Config.newInstance(getApplicationContext());
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Montserrat-Medium.otf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        setContentView(R.layout.activity_main);
        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        setTitle(getResources().getString(R.string.app_name));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

            }
        });
        Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.mipmap.ic_navigation_drawer, getTheme());
        getSupportActionBar().setHomeAsUpIndicator(drawable);
        drawerFragment = (FragmentDrawer)
                getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), mToolbar);
        drawerFragment.setDrawerListener(this);
        replaceFragment(R.id.container_body, new HomeFragment(), getString(R.string.app_name));
        linearLayout = (LinearLayout) findViewById(R.id.container_toolbar);
        Glide.with(this).load(R.drawable.bg_img_main).asBitmap().into(new SimpleTarget<Bitmap>() {
            @Override
            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                BitmapDrawable drawable2 = new BitmapDrawable(getResources(), Utils.createBitmap_ScriptIntrinsicBlur(MainActivity.this, resource, 50));
                linearLayout.setBackground(drawable2);
            }
        });


        // display the first navigation drawer view on app launch
        postIds();

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.playlist) {
            startActivity(new Intent(MainActivity.this, PlayListActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);
    }

    private void displayView(int position) {
        Fragment fragment = null;
        String title = getString(R.string.app_name);
        switch (position) {
            case 0:

                break;
            case 1:
                startActivity(new Intent(MainActivity.this, PlayListActivity.class));
                break;
            case 2:
                if (mConfig.getApiKey().equals("")) {
                    startActivity(new Intent(MainActivity.this, LoginGoogleActivity.class));
                } else {
                    startActivity(new Intent(MainActivity.this, RecentMusicActivity.class));
                }
                break;
            case 3:

                break;
            case 4:
                startActivity(new Intent(MainActivity.this, HelpActivity.class));
                break;
            case 5:
                startActivity(new Intent(MainActivity.this, SettingsActivity.class));
                break;
            case 6:
                startActivity(new Intent(MainActivity.this, LoginGoogleActivity.class).putExtra("logout", true));

                break;
            default:
                break;
        }

        if (fragment != null) {
            replaceFragment(R.id.container_body, fragment, title);


        }
    }

    protected void replaceFragment(@IdRes int containerViewId,
                                   @NonNull Fragment fragment, String title) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(containerViewId, fragment)
                .addToBackStack(title)
                .commit();
        getSupportActionBar().setTitle(title);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
                this.finish();
                return false;
            } else {
                drawerFragment = (FragmentDrawer)
                        getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
                drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), mToolbar);
                drawerFragment.setDrawerListener(this);
                drawerFragment.setDrawerState(true);
                replaceFragment(R.id.container_body, new HomeFragment(), getString(R.string.app_name));
                getSupportFragmentManager().popBackStack();
                removeCurrentFragment();

                return false;
            }


        }

        return super.onKeyDown(keyCode, event);
    }


    public void removeCurrentFragment() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        Fragment currentFrag = getSupportFragmentManager().findFragmentById(R.id.container_body);

        if (currentFrag != null)
            transaction.remove(currentFrag);

        transaction.commit();


    }


    void postIds() {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> postIds = apiService.postIds(serializeToJson(new MyDatabase(MainActivity.this).reternIds()));
        postIds.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200) {

                    try {
                        System.out.println(response.body().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                } else {
                    System.out.println(response.code());
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });


    }

    public String serializeToJson(ArrayList<String> ids) {
        Gson gson = new Gson();
        String j = gson.toJson(ids);
        return j;
    }


    @Override
    public void genreFilter() {
        EventBus.getDefault().post(new MessageEvent());
    }

    @Override
    public void onBackPressed() {


    }
}