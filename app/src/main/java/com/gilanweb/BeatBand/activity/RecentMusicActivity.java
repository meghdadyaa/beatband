package com.gilanweb.BeatBand.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.gilanweb.BeatBand.R;
import com.gilanweb.BeatBand.Retrofit.Model.ApiClient;
import com.gilanweb.BeatBand.Retrofit.Model.ApiInterface;
import com.gilanweb.BeatBand.Retrofit.Model.Order;
import com.gilanweb.BeatBand.Utils.Config;
import com.gilanweb.BeatBand.adapter.OrdersAdapter;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class RecentMusicActivity extends AppCompatActivity {
    Toolbar mToolbar;
    private static Config mConfig;
    RecyclerView recyclerView;
    OrdersAdapter ordersAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mConfig = Config.newInstance(getApplicationContext());
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Montserrat-Medium.otf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        setContentView(R.layout.activity_recent_music);
        mToolbar = findViewById(R.id.toolbar_recent_music);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        recyclerView = findViewById(R.id.other_beat_recent);
        getOrders();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_recent_music, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.log_out) {
            startActivity(new Intent(RecentMusicActivity.this, LoginGoogleActivity.class).putExtra("logout", true));

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent goToMainActivity = new Intent(getApplicationContext(), MainActivity.class);
        goToMainActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(goToMainActivity);
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    void getOrders() {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<Order> orderCall = apiService.getOrders(mConfig.getApiKey());
        orderCall.enqueue(new Callback<Order>() {
            @Override
            public void onResponse(Call<Order> call, Response<Order> response) {

                if (response.code() == 200) {
                    if (response.body().getOrders().size() > 0) {
                        ordersAdapter = new OrdersAdapter(response.body().getOrders(), RecentMusicActivity.this);
                        recyclerView.setLayoutManager(new LinearLayoutManager(RecentMusicActivity.this));
                        recyclerView.setAdapter(ordersAdapter);

                    } else {
                        Log.i("response:", "arraySize zero");
                    }

                } else if(response.code()==500) {
                    startActivity(new Intent(RecentMusicActivity.this, LoginGoogleActivity.class).putExtra("logout", true));
                }else {
                    System.out.println(response.code());
                }

            }

            @Override
            public void onFailure(Call<Order> call, Throwable t) {
                Toast.makeText(RecentMusicActivity.this, "Problem connecting to server", Toast.LENGTH_SHORT).show();
            }
        });
    }


}
