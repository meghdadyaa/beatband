package com.gilanweb.BeatBand.activity.dialog;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.gilanweb.BeatBand.R;
import com.gilanweb.BeatBand.Retrofit.Model.ApiClient;
import com.gilanweb.BeatBand.Retrofit.Model.ApiInterface;
import com.gilanweb.BeatBand.Retrofit.Model.CheckExpaire;
import com.gilanweb.BeatBand.Retrofit.Model.Prices;
import com.gilanweb.BeatBand.Utils.Config;
import com.gilanweb.BeatBand.Utils.RecyclerTouchListener;
import com.gilanweb.BeatBand.Utils.StaticValues;
import com.gilanweb.BeatBand.activity.LoginGoogleActivity;
import com.gilanweb.BeatBand.adapter.PriceDialogAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class buyBeatDialog extends DialogFragment {

    RecyclerView dialogPricesRecyclerView;
    LinearLayoutManager DialogPricesLinearLayoutManager;
    PriceDialogAdapter priceDialogAdapter;
    ImageView imageView;
    Button btnBuy;
    Button btnCancel;
    private String beat_id;
    private String file_price;
    private String layer_price;
    private String wave_price;
    List<Prices> allpriceList;
    TextView beatName;
    String baet_code;
    private static Config mConfig;
    String choose_type;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_FRAME, android.R.style.Theme_Dialog);
    }

    @Override
    public void onStart() {
        super.onStart();
        Window window = getDialog().getWindow();
        WindowManager.LayoutParams params = window.getAttributes();
        params.dimAmount = 0.6f;
        window.setAttributes(params);
        window.setBackgroundDrawableResource(android.R.color.transparent);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_beat_buy, container, false);
        mConfig = Config.newInstance(getActivity());
        dialogPricesRecyclerView = view.findViewById(R.id.dialog_prices_rec);
        beatName = view.findViewById(R.id.dialog_beat_name);
        imageView = view.findViewById(R.id.dialog_beat_image);
        btnBuy = view.findViewById(R.id.btn_dialog_buy);
        btnCancel = view.findViewById(R.id.btn_dialog_cancel);
        btnBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    buy_onclick(beat_id, choose_type);
                } catch (Exception e) {
                    Toast.makeText(getActivity(), "No item selected", Toast.LENGTH_SHORT).show();
                }

            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        init();
        setDialogPosition();
        return view;
    }


    public void init() {
        allpriceList = new ArrayList<>();
        DialogPricesLinearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        dialogPricesRecyclerView.setLayoutManager(DialogPricesLinearLayoutManager);
        priceDialogAdapter = new PriceDialogAdapter(getActivity());
        Bundle bundle = getArguments();
        file_price = bundle.getString("file_price", "");
        layer_price = bundle.getString("layer_price", "");
        wave_price = bundle.getString("wave_price", "");
        beat_id = bundle.getString("beat_code");
        String beat_name = bundle.getString("beat_name", "");
        String image = bundle.getString("image", "");
        Prices prices = new Prices();
        prices.setId(beat_id);
        prices.setTitle("خرید بیت به صورت پیش فرض با فرمت WAV");
        prices.setDescrption("");
        prices.setPrice(wave_price);
        prices.setType("1");
        Prices prices1 = new Prices();
        prices1.setId(beat_id);
        prices1.setTitle("خرید بیت به همراه خروجی لاین به لاین پروژه");
        prices1.setDescrption(" (در این صورت شما میتوانید مطابق میل خودتان در بیت تغییرات مختصری اعمال کنید\u0003و بعد از ضبط و در پروسه ی میکس، خروجی با کیفیت تری داشته باشید)");
        prices1.setPrice(layer_price);
        prices1.setType("2");
        Prices prices2 = new Prices();
        prices2.setId(beat_id);
        prices2.setTitle("خرید بیت و حق سفارشی سازی");
        prices2.setDescrption("\u0003در این صورت بیت بند موظف خواهد بود، پروژه را مطابق سلیقه ی شما تغییر داده و\u0003به صورت لاین به لاین به شما تحویل دهد");
        prices2.setPrice(file_price);
        prices2.setType("3");
        allpriceList.add(prices);
        allpriceList.add(prices1);
        allpriceList.add(prices2);
        priceDialogAdapter.add(allpriceList);
        try {
            Glide.with(this).load(StaticValues.CoverUrl + image).asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE).override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL).into(new SimpleTarget<Bitmap>() {
                @Override
                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                    Drawable drawable = new BitmapDrawable(resource);
                    imageView.setImageDrawable(drawable);
//                        BitmapDrawable drawable2 = new BitmapDrawable(getResources(), Utils.createBitmap_ScriptIntrinsicBlur(NowPlayingActivity.this, resource, 50));
//                        coordinatorLayout.setBackground(drawable2);
                }
            });
        } catch (Exception e) {

        }
        beatName.setText(beat_name);
        dialogPricesRecyclerView.setAdapter(priceDialogAdapter);
        dialogPricesRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), dialogPricesRecyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                if (position==0){
                    choose_type = "1";
                }else if (position==1){
                    choose_type = "2";
                }else {
                    choose_type = "3";
                }


            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

    }


    public void buy_onclick(final String beat_id, final String beat_type) {

        if (!mConfig.getApiKey().equals("")) {
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);
            Call<CheckExpaire> postlogin = apiService.logincheck(mConfig.getApiKey());
            postlogin.enqueue(new Callback<CheckExpaire>() {
                @Override
                public void onResponse(Call<CheckExpaire> call, Response<CheckExpaire> response) {

                    if (response.body().getLoginexpire().equals("no-expire")) {
                        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("http://mybeatband.com/dashboard/pay/" + mConfig.getApiKey() + "/" + beat_id + "/" + beat_type));
                        startActivity(i);
                        getActivity().finish();

                    } else {
                        startActivity(new Intent(getActivity(), LoginGoogleActivity.class).putExtra("logout", true));
                    }

                }

                @Override
                public void onFailure(Call<CheckExpaire> call, Throwable t) {

                }
            });

        } else {
            startActivity(new Intent(getActivity(), LoginGoogleActivity.class));
        }


    }


    private void setDialogPosition() {
        Window window = getDialog().getWindow();
        window.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);

        WindowManager.LayoutParams params = window.getAttributes();
        params.y = dpToPx(60);
        window.setAttributes(params);
    }

    private int dpToPx(int dp) {
        DisplayMetrics metrics = getActivity().getResources().getDisplayMetrics();
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, metrics);
    }
}