package com.gilanweb.BeatBand.activity;

/**
 * Created by Ravi on 29/07/15.
 */

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.gilanweb.BeatBand.R;
import com.gilanweb.BeatBand.Retrofit.Model.ApiClient;
import com.gilanweb.BeatBand.Retrofit.Model.ApiInterface;
import com.gilanweb.BeatBand.Retrofit.Model.Beat;
import com.gilanweb.BeatBand.Retrofit.Model.GetSlides;
import com.gilanweb.BeatBand.Retrofit.Model.OtherBeats;
import com.gilanweb.BeatBand.Retrofit.Model.Slides;
import com.gilanweb.BeatBand.Retrofit.Model.SpecialBeats;
import com.gilanweb.BeatBand.Utils.DatePreferences;
import com.gilanweb.BeatBand.Utils.EndlessRecyclerViewScrollListener;
import com.gilanweb.BeatBand.Utils.MessageEvent;
import com.gilanweb.BeatBand.Utils.RecyclerTouchListener;
import com.gilanweb.BeatBand.Utils.StaticValues;
import com.gilanweb.BeatBand.activity.dialog.filterGenreDialog;
import com.gilanweb.BeatBand.adapter.ExclusiveAdapter;
import com.gilanweb.BeatBand.adapter.NewBeatListAdapter;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.support.v7.widget.RecyclerView.LayoutManager;
import static com.gilanweb.BeatBand.Utils.StaticValues.setBeatArrayList;
import static com.gilanweb.BeatBand.Utils.StaticValues.setSongIndex;


public class HomeFragment extends Fragment {
    RecyclerView otherMusicRecyclerview;
    NewBeatListAdapter otherMusicAdapter;
    RecyclerView recyclerView;
    ExclusiveAdapter mAdapter;
    CircleIndicator indicator;
    ViewPager viewPager;
    LayoutManager mLayoutManager;
    LayoutManager mLayoutManagerotherMusic;
    List<Beat> BeatList = new ArrayList<>();
    List<Beat> BeatList1 = new ArrayList<>();
    private EndlessRecyclerViewScrollListener scrollListener;
    private EndlessRecyclerViewScrollListener scrollListener1;
    boolean T;
    String className;
    Spinner genre;
    TextView spicalText;
    TextView otherText;
    TextView filterByGenre;
    OtherBeats otherBeatsList = new OtherBeats();
    DatePreferences preferences;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);
        recyclerView = rootView.findViewById(R.id.exclusive_beat);
        otherMusicRecyclerview = rootView.findViewById(R.id.other_beat);
        viewPager = rootView.findViewById(R.id.view_pager);
        indicator = rootView.findViewById(R.id.indicator);
        spicalText = rootView.findViewById(R.id.spical_text);
        otherText = rootView.findViewById(R.id.other_beat_text);
        preferences = new DatePreferences(getActivity());
        Typeface face = Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/Montserrat-Regular.ttf");
        spicalText.setTypeface(face);
        otherText.setTypeface(face);
        preferences.SetTagTime("new");
        preferences.setTagIds("0");
        preferences.SetTagPrice("0");
        getSlides();
        filterByGenre = rootView.findViewById(R.id.filter_by_genre);
        filterByGenre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (otherBeatsList != null) {
                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                    Fragment prev = getFragmentManager().findFragmentByTag("dialog");
                    if (prev != null) {
                        ft.remove(prev);
                    }
                    ft.addToBackStack(null);
                    DialogFragment dialogFragment = new filterGenreDialog();
                    Bundle bundle = new Bundle();
                    bundle.putString("genre", new Gson().toJson(otherBeatsList.getGenre()));
                    bundle.putString("price", new Gson().toJson(otherBeatsList.getAllprice()));
                    dialogFragment.setArguments(bundle);
                    dialogFragment.show(ft, "dialog");
                }

            }
        });
        //first
        mAdapter = new ExclusiveAdapter(BeatList, getActivity());
        mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(mAdapter);

        geExclusive(0);
        scrollListener = new EndlessRecyclerViewScrollListener((LinearLayoutManager) mLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {

                geExclusive(Integer.parseInt(mAdapter.BeatList.get(totalItemsCount - 1).getId()));
                //   System.out.println(Integer.parseInt(mAdapter.BeatList.get(totalItemsCount - 1).getId()));


            }
        };
        recyclerView.addOnScrollListener(scrollListener);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {

                setBeatArrayList(mAdapter.BeatList);
                setSongIndex(Long.valueOf(position));
                startActivity(new Intent(getActivity(), NowPlayingActivity.class).putExtra("class", "HomeFragment"));

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        //secend

        otherMusicAdapter = new NewBeatListAdapter(BeatList1, getActivity());
        mLayoutManagerotherMusic = new GridLayoutManager(getActivity(), 2);
        otherMusicRecyclerview.setItemViewCacheSize(20);
        otherMusicRecyclerview.setDrawingCacheEnabled(true);
        otherMusicRecyclerview.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        otherMusicRecyclerview.setLayoutManager(mLayoutManagerotherMusic);
        otherMusicRecyclerview.setNestedScrollingEnabled(false);
        otherMusicRecyclerview.setAdapter(otherMusicAdapter);
        geOtherMusic(0, preferences.getTagsIds(), preferences.getTagPrice(), preferences.getTagTime());
        scrollListener1 = new EndlessRecyclerViewScrollListener((GridLayoutManager) mLayoutManagerotherMusic) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                System.out.println(otherMusicAdapter.BeatList.get(0).getId());

                geOtherMusic(Integer.parseInt(otherMusicAdapter.BeatList.get(0).getId()), preferences.getTagsIds(), preferences.getTagPrice(), preferences.getTagTime());


            }
        };

        otherMusicRecyclerview.addOnScrollListener(scrollListener1);
        otherMusicRecyclerview.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), otherMusicRecyclerview, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                setBeatArrayList(otherMusicAdapter.BeatList);
                setSongIndex(Long.valueOf(position));
                startActivity(new Intent(getActivity(), NowPlayingActivity.class).putExtra("class", "HomeFragment"));

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        return rootView;
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }


    public void geExclusive(int id) {


        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<SpecialBeats> getAllCall = apiService.getAll(id);
        getAllCall.enqueue(new Callback<SpecialBeats>() {


            @Override
            public void onResponse(Call<SpecialBeats> call, final Response<SpecialBeats> response) {
                System.out.println(response.body().getBeatList().size());

                if (!response.body().getBeatList().isEmpty())
                    mAdapter.add(response.body().getBeatList());


            }

            @Override
            public void onFailure(Call<SpecialBeats> call, Throwable t) {

            }
        });


    }

    public void geOtherMusic(int lastID, String genreID, String maxPrice, String orderBy) {


        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<OtherBeats> getAllCall = apiService.getother(lastID, genreID, maxPrice, orderBy);
        getAllCall.enqueue(new Callback<OtherBeats>() {


            @Override
            public void onResponse(Call<OtherBeats> call, final Response<OtherBeats> response) {

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (response.body().getBeatList() != null)
                                if (!response.body().getBeatList().isEmpty()) {
                                    otherMusicAdapter.add(response.body().getBeatList());
                                    otherBeatsList = response.body();
                                }
                        } catch (Exception e) {
                            Toast.makeText(getActivity(), "there is no item with this filter", Toast.LENGTH_SHORT).show();
                        }


                    }
                }, 2000);

            }

            @Override
            public void onFailure(Call<OtherBeats> call, Throwable t) {

                System.out.println(t.fillInStackTrace());

            }
        });


    }


    public void getSlides() {


        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<GetSlides> getAllCall = apiService.getSlides();
        getAllCall.enqueue(new Callback<GetSlides>() {

            @Override
            public void onResponse(Call<GetSlides> call, Response<GetSlides> response) {
                final ImagePagerAdapter adapter = new ImagePagerAdapter(response.body().getSlidesList());
                viewPager.setAdapter(adapter);
                indicator.setViewPager(viewPager);

                final Handler handler = new Handler();

                final Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        int i = 0;
                        // Insert custom code here
                        if (T) {
                            if (i < adapter.getCount()) {
                                i = viewPager.getCurrentItem();
                                i++;
                                if (i == adapter.getCount()) {
                                    i = 0;
                                    viewPager.setCurrentItem(i);
                                }
                            }
                        } else {
                            T = true;
                        }
                        viewPager.setCurrentItem(i);
                        handler.postDelayed(this, 4000);
                    }
                };

                handler.post(runnable);

                adapter.registerDataSetObserver(indicator.getDataSetObserver());


            }

            @Override
            public void onFailure(Call<GetSlides> call, Throwable t) {
                internetError();
            }
        });

    }

    @org.greenrobot.eventbus.Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        otherMusicAdapter.clear();
        geOtherMusic(0, preferences.getTagsIds(), preferences.getTagPrice(), preferences.getTagTime());
        System.out.println(preferences.getTagsIds());
        System.out.println(preferences.getTagPrice());
        System.out.println(preferences.getTagTime());
    }


    private class ImagePagerAdapter extends PagerAdapter {
        private List<Slides> mImages;

        ImagePagerAdapter(List<Slides> mImages) {
            this.mImages = mImages;
        }

        @Override
        public int getCount() {
            return mImages.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(final ViewGroup container, final int position) {
            Context context = getActivity();

            final ImageView imageView = new ImageView(context);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        if (!mImages.get(position).getLink().equals("")) {
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setData(Uri.parse(mImages.get(position).getLink()));
                            startActivity(intent);
                        }
                    } catch (Exception e) {

                    }

                }
            });
            Glide.with(getActivity()).load(StaticValues.SlidesUrl + mImages.get(position).getImage()).into(imageView);
            container.addView(imageView, 0);

            return imageView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager) container).removeView((ImageView) object);
        }


    }

    public void internetError() {
        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();

        alertDialog.setTitle("Info");
        alertDialog.setMessage("Internet not available, Cross check your internet connectivity and try again");
        alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                getActivity().finish();
            }
        });

        alertDialog.show();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        preferences.SetTagTime("new");
        preferences.setTagIds("0");
        preferences.SetTagPrice("0");
    }

    @Override
    public void onStop() {
        super.onStop();
        preferences.SetTagTime("new");
        preferences.setTagIds("0");
        preferences.SetTagPrice("0");
        EventBus.getDefault().unregister(this);
    }


}
