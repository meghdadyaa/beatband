package com.gilanweb.BeatBand;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;



/**
 * Created by meghdadya on 10/30/17.
 */

public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();


    }

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        MultiDex.install(this);
    }

}
