package com.gilanweb.BeatBand.services;


import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.gilanweb.BeatBand.Retrofit.Model.Beat;
import com.gilanweb.BeatBand.Utils.Config;
import com.gilanweb.BeatBand.activity.MainActivity;
import com.gilanweb.BeatBand.receivers.ControlActionsListener;
import com.gilanweb.BeatBand.receivers.HeadsetPlugReceiver;
import com.gilanweb.BeatBand.receivers.IncomingCallReceiver;
import com.gilanweb.BeatBand.receivers.RemoteControlReceiver;
import com.squareup.otto.Bus;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import static com.gilanweb.BeatBand.Utils.StaticValues.beatArrayList;

public class MusicService extends Service
        implements MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener, MediaPlayer.OnCompletionListener {
    private static final String TAG = MusicService.class.getSimpleName();
    private static final int MIN_DURATION_MS = 20000;
    private static final int PROGRESS_UPDATE_INTERVAL = 1000;
    private static final int NOTIFICATION_ID = 78;
    HomeWatcher mHomeWatcher = new HomeWatcher(this);
    private static HeadsetPlugReceiver mHeadsetPlugReceiver;
    private static IncomingCallReceiver mIncomingCallReceiver;
    private static MediaPlayer mPlayer;
    private static ArrayList<Integer> mPlayedSongIndexes;
    private Beat mCurrSong;
    private static Bus mBus;
    private static Config mConfig;
    private static Handler mProgressHandler;
    private static PendingIntent mPreviousIntent;
    private static PendingIntent mNextIntent;
    private static PendingIntent mPlayPauseIntent;

    private static boolean mWasPlayingAtCall;

    @Override
    public void onCreate() {
        super.onCreate();

        if (mBus == null) {
            mBus = BusProvider.getInstance();
            mBus.register(this);
        }

        mProgressHandler = new Handler();
        final ComponentName remoteControlComponent = new ComponentName(getPackageName(), RemoteControlReceiver.class.getName());
        final AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        audioManager.registerMediaButtonEventReceiver(remoteControlComponent);
    }

    private void initService() {
        mConfig = Config.newInstance(getApplicationContext());
        mPlayedSongIndexes = new ArrayList<>();
        mCurrSong = null;
        setupIntents();
        mHeadsetPlugReceiver = new HeadsetPlugReceiver();
        mIncomingCallReceiver = new IncomingCallReceiver(this);
        mWasPlayingAtCall = false;
        initMediaPlayerIfNeeded();
        //setupNotification();
    }

    private void setupIntents() {
        mPreviousIntent = getIntent(Constants.PREVIOUS);
        mNextIntent = getIntent(Constants.NEXT);
        mPlayPauseIntent = getIntent(Constants.PLAYPAUSE);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        mHomeWatcher.setOnHomePressedListener(new OnHomePressedListener() {
            @Override
            public void onHomePressed() {
                // do something here...
                if (isPlaying()) {
                    pauseSong();
                }

            }

            @Override
            public void onHomeLongPressed() {
            }
        });
        mHomeWatcher.startWatch();

        final String action = intent.getAction();
        if (action != null) {
            switch (action) {
                case Constants.INIT:

                    break;
                case Constants.PREVIOUS:
                    playPreviousSong();
                    break;
                case Constants.PAUSE:
                    pauseSong();
                    break;
                case Constants.PLAYPAUSE:

                    if (isPlaying()) {
                        pauseSong();
                    } else {
                        resumeSong();
                    }

                    break;
                case Constants.NEXT:
                    playNextSong();
                    break;
                case Constants.PLAYPOS:
                    if (mCurrSong == null)
                        initService();
                    playSong(intent);
                    break;
                case Constants.CALL_START:
                    incomingCallStart();
                    break;
                case Constants.CALL_STOP:
                    incomingCallStop();
                    break;
                case Constants.FINISH:
                    mBus.post(new Events.ProgressUpdated(0));
                    destroyPlayer();
                    break;
                case Constants.SET_PROGRESS:
                    if (mPlayer != null) {
                        final int progress = intent.getIntExtra(Constants.PROGRESS, mPlayer.getCurrentPosition() / 1000);
                        updateProgress(progress);
                    }
                    break;
                default:
                    break;
            }
        }

        return START_NOT_STICKY;
    }

    public void initMediaPlayerIfNeeded() {
        if (mPlayer != null)
            return;

        mPlayer = new MediaPlayer();
        mPlayer.setWakeMode(getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK);
        mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mPlayer.setOnPreparedListener(this);
        mPlayer.setOnCompletionListener(this);
        mPlayer.setOnErrorListener(this);

    }


  /*  private void setupNotification() {
        final String title = (mCurrSong == null) ? "" : mCurrSong.getName();
        final int playPauseButtonPosition = 1;
        final int nextButtonPosition = 2;
        int playPauseIcon = R.mipmap.play;
        if (isPlaying()) {
            playPauseIcon = R.mipmap.pause;
        }

        long when = 0;
        boolean showWhen = false;
        boolean usesChronometer = false;
        boolean ongoing = false;
        if (isPlaying()) {
            when = System.currentTimeMillis() - mPlayer.getCurrentPosition();
            showWhen = true;
            usesChronometer = true;
            ongoing = true;
        }


        final NotificationCompat.Builder notification = (NotificationCompat.Builder) new NotificationCompat.Builder(this).
                setStyle(new NotificationCompat.MediaStyle()
                        .setShowActionsInCompactView(new int[]{playPauseButtonPosition, nextButtonPosition})).
                setContentTitle(title).
                setSmallIcon(R.mipmap.speakers).
                setVisibility(Notification.VISIBILITY_PUBLIC).
                setPriority(Notification.PRIORITY_MAX).
                setWhen(when).
                setShowWhen(showWhen).
                setUsesChronometer(usesChronometer).
                setContentIntent(getContentIntent()).
                setOngoing(ongoing).
                addAction(R.mipmap.previous, "previous", mPreviousIntent).
                addAction(playPauseIcon, "playpause", mPlayPauseIntent).
                addAction(R.mipmap.next, "next", mNextIntent);



        startForeground(NOTIFICATION_ID, notification.build());

        if (!isPlaying()) {
            stopForeground(false);
        }
    }*/


    private PendingIntent getContentIntent() {
        final Intent contentIntent = new Intent(this, MainActivity.class);
        return PendingIntent.getActivity(this, 0, contentIntent, 0);
    }

    private PendingIntent getIntent(String action) {
        final Intent intent = new Intent(this, ControlActionsListener.class);
        intent.setAction(action);
        return PendingIntent.getBroadcast(getApplicationContext(), 0, intent, 0);
    }

    private int getNewSongId() {
        if (mConfig.getIsShuffleEnabled()) {
            final int cnt = beatArrayList.size();
            if (cnt == 0) {
                return -1;
            } else if (cnt == 1) {
                return 0;
            } else {
                final Random random = new Random();
                int newSongIndex = random.nextInt(cnt);
                while (mPlayedSongIndexes.contains(newSongIndex)) {
                    newSongIndex = random.nextInt(cnt);
                }
                return newSongIndex;
            }
        } else {
            if (mPlayedSongIndexes.isEmpty()) {
                return 0;
            }

            final int lastIndex = mPlayedSongIndexes.get(mPlayedSongIndexes.size() - 1);
            return (lastIndex + 1) % beatArrayList.size();
        }
    }

    public boolean isPlaying() {
        return mPlayer != null && mPlayer.isPlaying();
    }

    public void playPreviousSong() {
        if (beatArrayList.isEmpty())
            return;

        initMediaPlayerIfNeeded();

        // play the previous song if we are less than 5 secs into the song, else restart
        // remove the latest song from the list
        if (mPlayedSongIndexes.size() > 1 && mPlayer.getCurrentPosition() < 5000) {
            mPlayedSongIndexes.remove(mPlayedSongIndexes.size() - 1);
            setSong(mPlayedSongIndexes.get(mPlayedSongIndexes.size() - 1), false);
        } else {
            restartSong();
        }
    }

    public void pauseSong() {
        try {
            mPlayer.pause();
            songStateChanged(false);
        }catch (Exception e){

        }

    }

    public void resumeSong() {
        mPlayer.start();
        songStateChanged(true);
    }

    public void playNextSong() {
        setSong(getNewSongId(), true);
    }

    private void restartSong() {
        mPlayer.seekTo(0);
        // setupNotification();
    }

    private void playSong(Intent intent) {
        final int pos = intent.getIntExtra(Constants.SONG_POS, 0);
        setSong(pos, true);
    }

    public void setSong(int songIndex, boolean addNewSong) {
        if (beatArrayList.isEmpty())
            return;

        final boolean wasPlaying = isPlaying();
        initMediaPlayerIfNeeded();

        mPlayer.reset();
        if (addNewSong) {
            mPlayedSongIndexes.add(songIndex);
            if (mPlayedSongIndexes.size() >= beatArrayList.size()) {
                mPlayedSongIndexes.clear();
            }
        }

        mCurrSong = beatArrayList.get(songIndex);
        Log.i("mCurrSong", mCurrSong.getName());
        mBus.post(new Events.SongChanged(mCurrSong));
        try {


            mPlayer.setDataSource("http://mybeatband.com/dashboard/beats/" + mCurrSong.getBeat());
            mPlayer.prepareAsync();


            songStateChanged(true);

        } catch (IOException e) {
            Log.e(TAG, "setSong IOException " + e.getMessage());
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        if (mConfig.getRepeatSong()) {
            mPlayer.seekTo(0);
            mPlayer.start();
            //   setupNotification();
        } else if (mPlayer.getCurrentPosition() > 0) {
            mPlayer.reset();
            playNextSong();
        }
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        mPlayer.reset();
        return false;
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        mp.start();
        //  setupNotification();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        destroyPlayer();
        mHomeWatcher.stopWatch();
    }

    private void destroyPlayer() {
        if (mPlayer != null) {
            mPlayer.stop();
            mPlayer.release();
            mPlayer = null;
        }

        if (mBus != null) {
            songStateChanged(false);
            mBus.post(new Events.SongChanged(null));
            mBus.unregister(this);
        }


        final TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        telephonyManager.listen(mIncomingCallReceiver, PhoneStateListener.LISTEN_NONE);

        stopForeground(true);
        stopSelf();
    }

    public void incomingCallStart() {
        if (isPlaying()) {
            mWasPlayingAtCall = true;
            pauseSong();
        } else {
            mWasPlayingAtCall = false;
        }
    }

    public void incomingCallStop() {
        if (mWasPlayingAtCall)
            resumeSong();

        mWasPlayingAtCall = false;
    }

    private void updateProgress(int progress) {
        mPlayer.seekTo(progress * 1000);
        resumeSong();
    }

    private void songStateChanged(boolean isPlaying) {
        handleProgressHandler(isPlaying);
        //  setupNotification();
        mBus.post(new Events.SongStateChanged(isPlaying));

        if (isPlaying) {
            IntentFilter filter = new IntentFilter(Intent.ACTION_HEADSET_PLUG);
            registerReceiver(mHeadsetPlugReceiver, filter);

            final TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            telephonyManager.listen(mIncomingCallReceiver, PhoneStateListener.LISTEN_CALL_STATE);
        } else {
            try {
                unregisterReceiver(mHeadsetPlugReceiver);
            } catch (IllegalArgumentException e) {
                Log.e(TAG, "IllegalArgumentException " + e.getMessage());
            }
        }
    }

    private void handleProgressHandler(final boolean isPlaying) {
        if (isPlaying) {
            mProgressHandler.post(new Runnable() {
                @Override
                public void run() {
                    final int secs = mPlayer.getCurrentPosition() / 1000;
                    mBus.post(new Events.ProgressUpdated(secs));
                    mProgressHandler.removeCallbacksAndMessages(null);
                    mProgressHandler.postDelayed(this, PROGRESS_UPDATE_INTERVAL);
                }
            });
        } else {
            mProgressHandler.removeCallbacksAndMessages(null);
        }
    }
}
