package com.gilanweb.BeatBand.services;

import com.gilanweb.BeatBand.Retrofit.Model.Beat;

import java.util.List;

public class Events {
    public static class SongChanged {
        private static Beat mSong;

        SongChanged(Beat song) {
            mSong = song;
        }

        public Beat getSong() {
            return mSong;
        }
    }

    public static class SongStateChanged {
        private static boolean mIsPlaying;

        SongStateChanged(boolean isPlaying) {
            mIsPlaying = isPlaying;
        }

        public boolean getIsPlaying() {
            return mIsPlaying;
        }
    }

    public static class PlaylistUpdated {
        private static List<Beat> mSongs;

        PlaylistUpdated(List<Beat> songs) {
            mSongs = songs;
        }

        public List<Beat> getSongs() {
            return mSongs;
        }
    }

    public static class ProgressUpdated {
        private static int mProgress;

        ProgressUpdated(int progress) {
            mProgress = progress;
        }

        public int getProgress() {
            return mProgress;
        }
    }
}
