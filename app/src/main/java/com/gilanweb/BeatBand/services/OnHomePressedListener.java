package com.gilanweb.BeatBand.services;

/**
 * Created by meghdadya on 9/25/16.
 */
public interface OnHomePressedListener {

    void onHomePressed();

    void onHomeLongPressed();
}
